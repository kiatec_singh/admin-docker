export class Message {
  content: string;
  timestamp: Date;
  avatar: string;

  //constructor(content: string, avatar: string, timestamp?: Date){

  constructor(input){
    this.content = input.content;
    this.timestamp = input.timestamp;
    this.avatar = input.avatar;
  }
}