export class Localization {
    public localizationobj: any = {};
    constructor() {
        this.localizationobj.pagergotopagestring = "Gå til:";
        this.localizationobj.pagershowrowsstring = "Antal pr. side:";
        this.localizationobj.pagerrangestring = " af ";
        this.localizationobj.pagerfirstbuttonstring ="første";
        this.localizationobj.pagerlastbuttonstring="sidste";
        this.localizationobj.pagernextbuttonstring = "næste";
        this.localizationobj.pagerpreviousbuttonstring = "forrige";
        this.localizationobj.sortascendingstring = "Sorter stigende";
        this.localizationobj.sortdescendingstring = "Sorter faldende";
        this.localizationobj.sortremovestring = "Fjern sortering";
        this.localizationobj.filterchoosestring = "Alle";
        this.localizationobj.loadtext = "Indlæser..";
        this.localizationobj.filterselectallstring = "Alle";
        this.localizationobj.emptydatastring = "ingen oplysninger fundet";
        this.localizationobj.groupsheaderstring = "Træk en kolonneoverskrift hertil for at gruppere";
        this.localizationobj.filterselectstring = "Filtrer";
        this.localizationobj.filterstring = "Filtrer";
        this.localizationobj.todaystring = "<b>I dag<b>";
        this.localizationobj.clearstring = "<b>Ryd</b>";
        this.localizationobj.filterstringcomparisonoperators= ['contains', 'does not contain'];
        this.localizationobj.filternumericcomparisonoperators= ['less than', 'greater than'];
        this.localizationobj.filterdatecomparisonoperators=['less than', 'greater than'];
        this.localizationobj.filterbooleancomparisonoperators=['equal', 'not equal'];
        this.localizationobj.filterstringcomparisonoperators = ['tom', 'ikke tom', 'indeholder', 'indeholder (match tilfælde)', 'indeholder ikke ', ' indeholder ikke (match tilfælde) ', ' starter med ', ' starter med(match tilfælde)', 'slutter med ', ' slutter med (match tilfælde) ', ' lige ', ' lige (match tilfælde) ', ' null ', ' ikke null'];
        this.localizationobj.filternumericcomparisonoperators = ['lige', 'ikke lige', 'mindre end', 'mindre end eller lige', 'større end', 'større end eller lige', 'null', 'ikke null'];
        this.localizationobj.filterdatecomparisonoperators = ['lige', 'ikke lige', 'mindre end', 'mindre end eller lige', 'større end', 'større end eller lige', 'null', 'ikke null'];
        this.localizationobj.filterbooleancomparisonoperators = ['lige', 'ikke lige'];
        this.localizationobj.days = {
            names: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"],
            namesAbbr: ["Søn", "Man", "Tir", "Ons", "Tors", "Fre", "Lør"],
            namesShort: ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"]
        };
        this.localizationobj.months = {
            names: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December", ""],
            namesAbbr: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
        };
        this.localizationobj.browseButton = "gennemse"; 
    	this.localizationobj.uploadButton= "Upload"; 
    	this.localizationobj.cancelButton= "Afbestille"; 
    	this.localizationobj.uploadFileTooltip="Upload"; 
        this.localizationobj.cancelFileTooltip= "Afbestille"; 
        this.localizationobj.calendars ={
            standard: {
                "/": "-",
                firstDay: 1,
                days: {
                    names: ["søndag","mandag","tirsdag","onsdag","torsdag","fredag","lørdag"],
                    namesAbbr: ["sø","ma","ti","on","to","fr","lø"],
                    namesShort: ["sø","ma","ti","on","to","fr","lø"]
                },
                months: {
                    names: ["januar","februar","marts","april","maj","juni","juli","august","september","oktober","november","december",""],
                    namesAbbr: ["jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec",""]
                },
                AM: null,
                PM: null,
                patterns: {
                    d: "dd-MM-yyyy",
                    D: "d. MMMM yyyy",
                    t: "HH:mm",
                    T: "HH:mm:ss",
                    f: "d. MMMM yyyy HH:mm",
                    F: "d. MMMM yyyy HH:mm:ss",
                    M: "d. MMMM",
                    Y: "MMMM yyyy"
                }
            }
        }
    }
}