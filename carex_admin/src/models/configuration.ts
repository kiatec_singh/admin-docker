export class Configuration {
    public apiAddress: string;
    public apiToken: string;
    public environment: string;
}