import { Configuration } from './configuration';

export class Environments {
    private static test: Configuration = {
        apiAddress: 'http://87.54.27.85/carex/carex_api/test/',
        apiToken: '',
        environment: 'test'
    };

    private static demo: Configuration = {
        apiAddress: 'http://87.54.27.85/carex/carex_api/demo/',
        apiToken: '',
        environment: 'demo'
    };

    private static prod: Configuration = {
        apiAddress: 'http://87.54.27.85/carex/carex_api/',
        apiToken: '',
        environment: 'prod'
    };

    private static dev: Configuration = {
        apiAddress: 'http://87.54.27.85/carex/carex_api/udv/',
        apiToken: '',
        environment: 'dev'
    };

    public static getTest() {
        return this.test;
    }

    public static getDemo() {
        return this.demo;
    }

    public static getProd() {
        return this.prod;
    }

    public static getDev() {
        return this.dev;
    }
}