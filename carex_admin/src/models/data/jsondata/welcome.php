
<?php

header("Access-Control-Allow-Origin: *");

$welcome = "{
    \"info\": {
        \"heading\": \"Velkommen til TRYG Sundhed\",
        \"logo\": \"logo-kia-300x88.jpg\",
        \"bg_image\": \"46795474.jpg\",
        \"main_color\": \"#D00000\",
        \"paragraphs\": [
            \"TRYG Sundhed giver dig kontrol over dine data, og lader dig selv bestemme hvem du deler dem med.\",
            \"Du får overblik over de velfærdsservices du har adgang til genne din arbejdsgiver, forsikringsselskab, bopælskommune, pensionsselskab, A-kasse m.fl. *)\",
            \"For at kunne vise dig de ydelser der er til rådighed for dig, skal vi være sikre på hvem du er. Derfor vil vi på de næste sider bede dig logge ind i systemet.\",
            \"For at bruge de services som du har til rådighed, kan det i nogle tilfælde være nødvendigt at dele dine personlige oplysninger med serviceudbyderen.\",
            \"TRYG Sundhed deler kun dine data når du har givet samtykke til det, og i så fald kun de data som er nødvendige, for den service du ønsker at gøre brug af.\"
        ],
        \"conditions\": [
            \"*) Forudsætter at din relation er tilsluttet platformen.\"
        ]
    }
}";

echo $welcome;
?>
