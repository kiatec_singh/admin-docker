export class UserData {
 
    public uuid: string;
    public emailid: string;
    public bruger_group: string;
    public type: string;
    public gyldighed: string;
    public username: string;
    constructor(input) {
        this.uuid = input.id;
        this.emailid = input.email;
        this.bruger_group = input.bruger_group;
        this.gyldighed = input.gyldighed;
        this.username = input.username;
    } 
}

// export class UserInfo{
//     public uuid = this.uuid;
//     public  emailid = this.emailid;
//     public bruger_group = this.bruger_group;
//     public gyldighed = this.gyldighed;
//     public username = this.username;
// }