import { Component, Output, Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';

declare var require: any;
declare var $: any;
var xlsx = require('json-as-xlsx');

@Component({
    selector: 'analytics-viewer',
    templateUrl: 'analytics.html'
})
export class analyticsComponent {

    private alldomainsdata: any;
    private eventData: any;
    private myColumnNames: any;
    private options: any;
    private pieoptions: any;
    private platformsoptions: any;
    private platformspieoptions: any;
    private versionscolumnoptions: any;
    private activeuserscolumnoptions: any;
    private userrelationscolumnoptions: any;
    private isLoading = true;


    private columnchart = "ColumnChart";
    private piechart = "PieChart";
    private donutchart = "donutchart";
    private barchart = "BarChart";
    private chartdata = [];
    private Dashboard = "Dashboard";
    private pagesevent;
    private allevents;
    private platforms;
    private userrelations;
    private platformsData: any;
    private activeusers = [];
    private totalusers = [];
    private activeusersData: any;
    private versions = [];
    private versionsData: any;
    private totalusersData: any;
    private userrelationsData: any;
    private filteredata: any;
    private slctapplication=[];
    private enddate = null;
    private startdate = null;
    private allapplications = [
        { value: "7fb031ae-0cb9-4a04-8407-1156d3d52108", appname: "Tryg", selected: false },
        { value: "bef7e721-29cb-4eec-b0a9-af70fdc7b05d", appname: "Molholm", selected: false },
        { value: "4f4645fc-7c9a-4de7-9592-145edafdfc4c", appname: "Willis", selected: false },
        { value: "8e7fcffe-444b-4468-ab59-9f63b00d15c5", appname: "Ap", selected: false },
    ];
    @Input() domainname: any;
    @Output() onChange = new EventEmitter()


    private analyticsForm: any;

    constructor(private fb: FormBuilder, private router: Router, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.analyticsForm = this.fb.group({
            application: ['', Validators.required],
            startdate: [''],
            enddate: ['']
        });
    }

    ngOnInit() {
        this.baserestService.getuseranalytics().then(
            (alldomainsdata) => {
                this.alldomainsdata = alldomainsdata;
                this.filteredata = alldomainsdata;
                this.createChart();
            }
        )
    }
    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    createChart() {
        //console.log(this.alldomainsdata);
        this.allevents = [];
        this.platforms = [];
        this.versions = [];
        this.versionsData = [];
        this.activeusers = [];
        this.totalusers = [];
        this.userrelations = [];
        if (this.filteredata && this.filteredata.length) {
            for (let i in this.filteredata) {
                if (this.filteredata[i].request) {
                    this.allevents.push(this.filteredata[i].request.eventname);
                    this.platforms.push(this.filteredata[i].request.platform);
                    this.versions.push(this.filteredata[i].request.version);
                    this.activeusers.push(this.filteredata[i].request.bruger_uuid);
                    this.userrelations.push(this.filteredata[i].request.applikation_uuid);
                    if (this.totalusers.indexOf(this.filteredata[i].request.autorisation) === -1) {
                        this.totalusers.push(this.filteredata[i].request.autorisation);
                    }
                }
            }
            var eventscount = {};
            var platformscount = {};
            var versionscount = {};
            var activeuserscount = {};
            var userrelationsscount = {};
            this.allevents.forEach(function (i) { eventscount[i] = (eventscount[i] || 0) + 1; });
            this.platforms.forEach(function (i) { platformscount[i] = (platformscount[i] || 0) + 1; });
            this.versions.forEach(function (i) { versionscount[i] = (versionscount[i] || 0) + 1; });
            this.activeusers.forEach(function (i) { activeuserscount[i] = (activeuserscount[i] || 0) + 1; });
            this.userrelations.forEach(function (i) { userrelationsscount[i] = (userrelationsscount[i] || 0) + 1; });

            this.makeeventchart(eventscount);
            this.makeplatformschart(platformscount);
            this.makeversionschart(versionscount);
            this.makeactiveusersschart(activeuserscount);
            this.makeuserbyrelationschart(userrelationsscount);
            this.totalusersData = this.totalusers;
        }
        else {
            this.platformsData = null;
            this.activeusersData = null;
            this.activeusersData = null;
            this.versionsData = null;
            this.userrelationsData = null;
            this.eventData = null;
        }
    }
    makeeventchart(count) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in count) {
            j = [i, count[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        // this.options = {
        //     is3D: true,
        //     width: 600,
        //     height: 600,
        //     bar: { groupWidth: "100%" },
        //     legend: { position: "none" },
        //     colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        // };
        this.pieoptions = {
            is3D: true,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
            //  colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.eventData = allformatedevent;
        //  this.isLoading=false;
    }
    makeplatformschart(platformscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in platformscount) {
            j = [i, platformscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.platformspieoptions = {
            pieHole: 0.5,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
        };
        this.platformsData = allformatedevent;
    }
    makeversionschart(versionscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.versionscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.versionsData = allformatedevent;
    }
    makeactiveusersschart(versionscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.activeuserscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.activeusersData = allformatedevent;
    }
    makeuserbyrelationschart(versionscount) {
        let data = [];
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        let appname = 'undefined';
        let appcolor = '#0d3348';
        for (let i in versionscount) {
            if (i === "7fb031ae-0cb9-4a04-8407-1156d3d52108") {
                appname = 'Tryg';
                appcolor = '#dc0000';
            }
            if (i === "bef7e721-29cb-4eec-b0a9-af70fdc7b05d") {
                appname = 'Mølholm Sundhedsunivers';
                appcolor = '#595959';
            }
            if (i === "8e7fcffe-444b-4468-ab59-9f63b00d15c5") {
                appname = 'Sundhed via AP';
                appcolor = '#ff4d1d';
            }
            if (i === "undefined") {
                appname = 'undefined';
                appcolor = '#0d3348';
            }

            j = [appname, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.userrelationscolumnoptions = {
            chartType: 'BarChart',
            type: 'BarChart',
            is3D: true,
            explorer: { axis: 'horizontal', keepInBounds: true },
            dataTable: allformatedevent,
            options: {
                is3D: true,
                width: 600,
                height: 400,
                bar: { groupWidth: "100%" },
                legend: { position: "none" },
            }
        };
        this.userrelationsData = allformatedevent;
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    handleChange(name) {
        console.log(name);
    }
    getanalyticsData() {
        console.log("oin submit");
    }
    StartDate(i, e) {
        this.getfiltereddata();
    }
    EndDate(i, e) {
        console.log(e);
        this.getfiltereddata();
    }
    clearstartDate(item) {
        console.log(item);
        this.startdate = null;
        this.getfiltereddata();
    }
    clearendDate(item) {
        this.enddate = null;
        this.getfiltereddata();
    }
    Multiselect(e, item) {
        this.getfiltereddata();
    }
    getfiltereddata() {
        this.filteredata = this.alldomainsdata;
        let tempfiltereddata = [];
        if (this.slctapplication && this.slctapplication.length && this.startdate === null && this.enddate === null) {
            tempfiltereddata = this.filteredata.filter(
                x => this.slctapplication.includes(x.applikation_uuid)
            );
        }
        if (this.slctapplication && this.slctapplication.length && this.startdate != null && this.enddate === null) {
            tempfiltereddata = this.filteredata.filter(
                x => this.slctapplication.includes(x.applikation_uuid) && new Date(x.log_timestamp) > this.startdate._d
            );
        }
        if (this.slctapplication && this.slctapplication.length && this.startdate === null && this.enddate != null) {
            tempfiltereddata = this.filteredata.filter(
                x => this.slctapplication.includes(x.applikation_uuid) && new Date(x.log_timestamp) < this.enddate._d
            );
        }
        if (this.slctapplication && this.slctapplication.length && this.startdate != null && this.enddate != null) {
            tempfiltereddata = this.filteredata.filter(
                x => this.slctapplication.includes(x.applikation_uuid) && new Date(x.log_timestamp) < this.enddate._d && new Date(x.log_timestamp) > this.startdate._d
            );
        }
        if (!this.slctapplication && this.startdate != null && this.enddate != null) {
            tempfiltereddata = this.filteredata.filter(
                x => new Date(x.log_timestamp) < this.enddate._d && new Date(x.log_timestamp) > this.startdate._d
            );
        }
        if (this.slctapplication && !this.slctapplication.length && this.startdate === null && this.enddate === null) {
            tempfiltereddata = this.alldomainsdata;
        }
        this.filteredata = tempfiltereddata;
        this.createChart();
    }

    // downloadservicelogs() {
    //     let startdate = null;
    //     let enddate = null;
    //     if (this.startdate && this.startdate._d) {
    //         var dd: any = this.startdate._d.getDate();
    //         var mm: any = this.startdate._d.getMonth() + 1;
    //         var yyyy = this.startdate._d.getFullYear();
    //         if (dd < 10) {
    //             dd = '0' + dd;
    //         }
    //         if (mm < 10) {
    //             mm = '0' + mm;
    //         }
    //         startdate = yyyy + '-' + mm + '-' + dd;
    //     }
    //     if (this.enddate && this.enddate._d) {
    //         var dd: any = this.enddate._d.getDate();
    //         var mm: any = this.enddate._d.getMonth() + 1;
    //         var yyyy = this.enddate._d.getFullYear();
    //         if (dd < 10) {
    //             dd = '0' + dd;
    //         }
    //         if (mm < 10) {
    //             mm = '0' + mm;
    //         }
    //         enddate = yyyy + '-' + mm + '-' + dd;
    //     }
    //     this.baserestService.getserviceanalytics(startdate,enddate).then(
    //         (servicelogsdata) => {
    //             console.log(servicelogsdata);
    //         }
    //     )
    // }

    download() {
        var excelcolumns = [];
        var exceldata = [];
        this.filteredata.map(function (item) {
            let timestamp = item.log_timestamp;
            let requestdata = item.request;
            requestdata.log_timestamp = timestamp;
            exceldata.push(requestdata);
        });
        excelcolumns.push({ label: 'action', value: 'action' });
        // excelcolumns.push({ label: 'name', value: 'name' });
        excelcolumns.push({ label: 'platform', value: 'platform' });
        excelcolumns.push({ label: 'autorisation', value: 'autorisation' });
        excelcolumns.push({ label: 'bruger_uuid', value: 'bruger_uuid' });
        excelcolumns.push({ label: 'eventname', value: 'eventname' });
        excelcolumns.push({ label: 'itsystem_uuid', value: 'itsystem_uuid' });
        excelcolumns.push({ label: 'applikation_uuid', value: 'applikation_uuid' });
        excelcolumns.push({ label: 'version', value: 'version' });
        excelcolumns.push({ label: 'relation_organizationid', value: 'relation_organizationid' });
        excelcolumns.push({ label: 'relation_organizationname', value: 'relation_organizationname' });
        excelcolumns.push({ label: 'log_timestamp', value: 'log_timestamp' });
        var settings = {
            sheetName: "Analytics",
            fileName: "Analytics",
        }
        xlsx(excelcolumns, exceldata, settings);
    }
}
