import { Component, Output,Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
// import { OtpPage } from '../../pages/otp/otp';
// import { LoginPage } from '../../pages/login/login.page';
import { MenuLeft } from '../menu-left/menu-left';


// declare let google: any;

@Component({
    selector: 'domainstatistics-viewer',
    templateUrl: 'domainstatistics.html'
})
export class domainstatisticsComponent {

    private alldomainsdata: any;
    private myData=[];
    private myColumnNames:any;
    private options:any;
    private pieoptions:any;
    private type="ColumnChart";
    private bartype="PieChart";
    private chartdata=[];
    @Input() domainname: any;
    private piedata:any

    constructor(private fb: FormBuilder, private router: Router, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {

     
    }

    ngOnInit() {
        // Tracking
        //this.environment = this.auth.getEnvironment();
        // this.getloginData();

        // setTimeout(() => {
        //       google.charts.load('current', { 'packages': ['bar'] });
        // google.charts.setOnLoadCallback(this.createChart());
        // }, 1000);
  
         this.baserestService.getdomainstatistics(this.domainname).then(
            (alldomainsdata) => {this.piedata = alldomainsdata;       this.createChart();
                 }
        )
    }
createChart() {
          this.pieoptions = {
            is3D: true,
            width: 700,
            height: 400,
            tooltip: { isHtml: true },
            colors: ['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99','#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],

          };
         
        
//          for(let d in this.alldomainsdata){
//             if(this.alldomainsdata[d].domain){
//                 this.chartdata.push([this.alldomainsdata[d].domain, +this.alldomainsdata[d].total])
//             }
//          }
//          console.log(this.chartdata);
//          console.log(this.myData);
// this.myData = this.chartdata;
let piechartdata=[];

piechartdata.push(["Active", this.piedata.Aktiv]);
piechartdata.push(["InActive", this.piedata.Nonaktiv]);

this.alldomainsdata = piechartdata;

console.log(this.alldomainsdata);
        
}

}
