import { Component, ViewChild } from '@angular/core';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Services } from '../../providers/services/services';
import {  ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { GridData } from '../../models/grid.data';


@Component({
  selector: 'ganttchart',
  templateUrl: 'ganttchart.html'
})

export class GanttChart {
  private chart: AmChart;
  private parentheading;
  private displayname;
  private heading;
  private uuid;
  private selectedMenu;
  private clickedrow;
  private userinformation;
  private gridrowsandColumns;
  private gridData: GridData;
  private chartData;
  private allcategories: any;
  private chartstartDate = null;
  private navigationElements;


  @ViewChild('chartdiv',{static:false}) chartdiv: any;

  constructor(private AmCharts: AmChartsService, private activatedRoute: ActivatedRoute, private services: Services, public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.parentheading = this.services.getselectedMenu();
    if (this.parentheading != "Personer") {
      this.parentheading = null;
    }

    this.parentheading = this.services.getselectedMenu();
    this.displayname = this.services.getdisplayName();
    this.heading = this.activatedRoute.snapshot.data.title;



    if (!this.activatedRoute.snapshot.data.issubmenu) {
      console.log("submenu false");
      this.services.setselectedMenu(this.heading);
    }

    //this.services.setselectedMenu(this.heading);
    this.selectedMenu = this.activatedRoute.snapshot.routeConfig.path;
    this.uuid = this.activatedRoute.snapshot.data.listId;
    this.services.setlistId(this.uuid);
    this.clickedrow = this.services.getClickedRowuuiid();
    console.log(this.activatedRoute.snapshot);

    //this.uuid = 'e4c4c23f-0c51-4ee4-8bc3-f70c27e5b786';//this.router.routerState.snapshot.root.queryParams['uuid'];
    console.log(this.uuid);
    //this.getValueFromObservable();
  }
  breadCrumb() {
    if (!this.activatedRoute.snapshot.data.issubmenu) {
      console.log("submenu false");
      this.services.clearNavigation();
      this.services.setselectedMenu(this.heading);
      if (this.services.getContext() != null) {
        this.services.removeNavigation();
      }
      this.services.addNavigation(this.heading);

    }
    if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() == null ) {
      console.log("submenu true");
      this.services.setselectedMenu(this.heading);
      if (this.services.getContext() == null && this.services.getNavigation().length > 1) {
        this.services.removeNavigation();
      }
      this.services.addNavigation(this.heading);
    }
    if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() != null ){
      if(this.services.getNavigation().length==3){
        this.services.removeNavigation();
     }
      this.services.addNavigation(this.heading);
    }

    this.navigationElements = this.services.getNavigation();


  }

  // getValueFromObservable(): void {
  //   this.breadCrumb();
  //   this.services.getGriddata(this.uuid, this.gridData,'', this.clickedrow).then(
  //     res => { this.gridrowsandColumns = res; this.setData() },
  //     error => { this.dataReceived() },
  //     complete => this.dataReceived()
  //   );
  // }
  setData() {
    console.log(this.gridrowsandColumns.result.objekt_liste);
    this.constructChart();
    let chartdata = this.gridrowsandColumns.result.objekt_liste;
    let formatteddata = [];
    let chartItem;
    let temparry = [];
    let segments;
    let chartdate;


    for (let i in chartdata) {
      console.log(chartdata[i].ansvarlig);
      if (chartdata[i].ansvarlig) {
        let startdate: any = this.datepipe.transform(chartdata[i].fra, 'yyyy-MM-dd');
        let enddate: any = this.datepipe.transform(chartdata[i].til, 'yyyy-MM-dd');
        // let duration = startdate - enddate;
        let task = chartdata[i].emne + '-' + chartdata[i].udfoerer;
        //let task = chartdata[i].brugervendtnoegle; 
        let startdatee = new Date(startdate);
        let enddatee = new Date(enddate);
        let duration = enddatee.getTime() - startdatee.getTime();
        this.chart
        //getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

        duration = Math.round(Math.abs(duration / (1000 * 60 * 60 * 24)));
        let segmentColor = "";

        //let startdateee = this.AmCharts.formatDate(new Date(2014, 1, 3), "YYYY MMM, YYYY");
        if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Mødeaktivitet") {
          segmentColor = "#46615e";
        }
        if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Udredningsaktivitet") {
          segmentColor = "#727d6f";
        }
        if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Indsatsaktivitet") {
          segmentColor = "#8dc49f";
        }
        if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Observationsaktivitet") {
          segmentColor = "#FFE4C4";
        }
        // let startdateCal = startdate.substring(startdate.lastIndexOf('-') + 1, startdate.length);
        let startdateCal = startdate.substring(startdate.indexOf('-') + 1, startdate.lastIndexOf('-'));
        console.log(startdateCal);

        chartItem = {
          "category": chartdata[i].ansvarlig,
          "segments": []
        }


        if (this.chartstartDate == null) {
          this.chartstartDate = startdatee
        }
        if (this.chartstartDate) {

          if (this.chartstartDate < startdatee) {

          }
          if (this.chartstartDate > startdatee) {
            this.chartstartDate = startdatee
          }


        }


        segments = {
          "start": startdatee,
          "duration": duration,
          "color": segmentColor,
          "task": task
        }
        // if(chartdata[i].brugervendtnoegle=="Udredning af Rasmus Geertsen"){
        //   segments = {
        //     "start": 900,
        //     "duration": 4,
        //     "color": segmentColor,
        //     "task": task
        //   }
        // }else{
        //   segments = {
        //     "start": startdateCal,
        //     "duration": duration,
        //     "color": segmentColor,
        //     "task": task
        //   }
        // }

        // if (formatteddata.filter(x => x.category == chartItem.category)[0]){
        //   console.log(formatteddata);
        //  return chartItem.segements;
        // }

        chartItem.segments.push(segments);

        if (formatteddata.length == 0) {
          formatteddata.push(chartItem);
        }


        if (formatteddata.length >= 1) {

          if (!formatteddata.filter(x => x.category == chartItem.category)[0]) {
            formatteddata.push(chartItem);
          }
          //    if (formatteddata.filter(x => x.category == chartItem.category)[0]) {

          formatteddata.filter(x => {
            if (x.category == chartItem.category) {

              if (chartItem.segments[0]) {
                if (x.segments[0].color != chartItem.segments[0].color) {
                  x.segments.push(chartItem.segments[0]);
                }

              }
            }

          });
          console.log(formatteddata)
          console.log(chartItem);
          // segments = {
          //   "start": startdate,
          //   "duration": 10,
          //   "color": "#46615e",
          //   "task": task
          // }





          //  x.segments.push(segments);
          //chartItem.segments.push(segments);


          console.log(chartItem.segements);
          console.log(formatteddata)
        }

        //}

      }
    }


    console.log(formatteddata);

    this.allcategories = formatteddata;

    this.constructChart();

  }
  constructChart() {
    console.log(this.chartstartDate);
    let chartiniitalDate = this.chartstartDate;
    if (this.chartstartDate) {
      let month = this.chartstartDate.getMonth() + 1;
      let date = this.chartstartDate.getDate();
      let year = this.chartstartDate.getFullYear();

      if (month < 10) {
        month = '0' + month;
      }
      if (date < 10) {
        date = '0' + date;
      }
      this.chartstartDate = year + '-' + month + '-' + date;
      this.chartstartDate.toString();
    }


    for (let i in this.allcategories) {

      for (let s in this.allcategories[i].segments) {
        let sdate = new Date(this.allcategories[i].segments[s].start);
        let edate = new Date(chartiniitalDate);
        let duration = sdate.getTime() - edate.getTime();


        duration = Math.round(Math.abs(duration / (1000 * 3600 * 24)));
        console.log(duration);
        this.allcategories[i].segments[s].start = duration;
      }
    }
    // let startdatee = new Date(startdate);
    // let enddatee = new Date(enddate);
    // let duration = enddatee.getTime() - startdatee.getTime();



    //  this.allcategories.map(function(val) {
    //   console.log(val);
    // });


    this.chart = this.AmCharts.makeChart("chartdiv", {
      "type": "gantt",
      "theme": "light",
      "marginRight": 70,
      "period": "DD",
      "dataDateFormat": "YYYY-MM-DD",
      "balloonDateFormat": "JJ:NN",
      "columnWidth": 0.5,
      "valueAxis": {
        "type": "date"
      },
      "brightnessStep": 10,
      "graph": {
        "fillAlphas": 1,
        "balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
      },
      "rotate": true,
      "categoryField": "category",
      "segmentsField": "segments",
      "colorField": "color",
      "startDate": this.chartstartDate ? this.chartstartDate : null,
      "startField": "start",
      "endField": "end",
      "durationField": "duration",
      "dataProvider": this.allcategories,
      "valueScrollbar": {
        "autoGridCount": true
      },
      "chartCursor": {
        "cursorColor": "#55bb76",
        "valueBalloonsEnabled": false,
        "cursorAlpha": 0,
        "valueLineAlpha": 0.5,
        "valueLineBalloonEnabled": true,
        "valueLineEnabled": true,
        "zoomable": false,
        "valueZoomable": true
      },
      "export": {
        "enabled": true
      }
    });


  }

  dataReceived(): void {
    console.log(this.gridrowsandColumns);
  }
  ngOnDestroy() {
    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
  }
}
