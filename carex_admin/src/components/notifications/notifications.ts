import {Component, OnInit, ViewChild} from '@angular/core'
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'notifications',
    templateUrl: 'notifications.html'
})

export class Notifications{

    @ViewChild('successNotification',{static:false}) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification',{static:false}) errorNotification: jqxNotificationComponent;
    @ViewChild('warningNotification',{static:false}) warningNotification: jqxNotificationComponent;
    @ViewChild('infoNotification',{static:false}) infoNotification: jqxNotificationComponent;
    constructor() {

    }
    ngOnInit() {

      }
     
      displayWarning(){
         this.warningNotification.open();

      }
      displaySuccess(){
        this.successNotification.open();

     }
     displayError(){
        this.errorNotification.open();

     }
     displayInfo(){
        this.infoNotification.open();

     }

}