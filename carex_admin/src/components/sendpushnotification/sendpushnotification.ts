import { Component, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
// import { OtpPage } from '../../pages/otp/otp';
// import { LoginPage } from '../../pages/login/login.page';
import { MenuLeft } from '../menu-left/menu-left';
import { Title } from '@angular/platform-browser';


@Component({
    selector: 'sendpushnotification-viewer',
    templateUrl: 'sendpushnotification.html'
})
export class sendpushnotificationComponent {


    private loginForm: FormGroup;
    private notificationSuccess;
    private android = false;
    private ios = false;
    private web = false;
    private selectedappId: any;
    private alldevices;
    private allregisteredusers;
    private isSelectall = false;
    private titlemodel = '';
    private messagemodel = '';
    @ViewChild('successNotification',{static:false}) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification',{static:false}) errorNotification: jqxNotificationComponent;

    @Output() loggedinuser = new EventEmitter();



    constructor(private fb: FormBuilder, private router: Router, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.loginForm = this.fb.group({
            title: ['', Validators.required],
            message: ['', Validators.required],
            ios: [false],
            android: [false],
            web: [false],
        });

    }

    ngOnInit() {
        // Tracking
        //this.environment = this.auth.getEnvironment();
        // this.getloginData();
        this.baserestService.getalldevices().then(
            alldevices => {
                //this.menulist = menulist.result;
                this.allregisteredusers = alldevices;
                this.alldevices = alldevices;
                console.log(alldevices);
            },
            error => {
                console.log(error)
            }
        )

    }
    selectApp(event) {
        let selectedApp = event.target.value;
        this.selectedappId = event.target.value;
        console.log(selectedApp);
        let allfiltereddevices = [];
        if (selectedApp != '0') {
            for (let i = 0; i < this.allregisteredusers.length; i++) {
                if (this.allregisteredusers[i].applikationuuid == selectedApp) {
                    allfiltereddevices.push(this.allregisteredusers[i])
                }

            }
            this.alldevices = allfiltereddevices;
        }
        else {
            this.baserestService.getalldevices().then(
                alldevices => {
                    //this.menulist = menulist.result;
                    this.allregisteredusers = alldevices;
                    this.alldevices = alldevices;
                    console.log(alldevices);
                },
                error => {
                    console.log(error)
                }
            )
        }



    }
    toogleAndroid(e) {
        e.preventDefault();
        // this.loginForm.value.ios = this.loginForm.value.ios === true ? false : true;

        let v = this.android === true ? false : true;
        this.android = v;
        console.log(this.android);
        // let allfiltereddevices = [];
        // this.alldevices = allfiltereddevices;
        // if (this.android === true) {
        //     for (let i = 0; i < this.alldevices.length; i++) {
        //         if (this.alldevices[i].platform === 'android') {
        //             allfiltereddevices.push(this.alldevices[i])
        //         }
        //         this.alldevices = allfiltereddevices;
        //     }
        // }
        // else {
        //     for (let i = 0; i < this.allregisteredusers.length; i++) {
        //         if (this.allregisteredusers[i].applikationuuid == this.selectApp) {
        //             allfiltereddevices.push(this.allregisteredusers[i])
        //         }

        //     }
        //     this.alldevices = allfiltereddevices;

        // }

    }
    selectAll(e) {
        console.log(this.isSelectall);
    }
    toogleIos(e) {
        e.preventDefault();
        // this.loginForm.value.android = this.loginForm.value.android === true ? false : true;
        let v = this.ios === true ? false : true;
        this.ios = v;
        console.log(this.ios);
        let allfiltereddevices = [];
        if (this.ios === true) {
            for (let i = 0; i < this.alldevices.length; i++) {
                if (this.alldevices[i].platform === 'ios') {
                    allfiltereddevices.push(this.alldevices[i])
                }
                this.alldevices = allfiltereddevices;
            }
        }
        else {
            for (let i = 0; i < this.allregisteredusers.length; i++) {
                if (this.allregisteredusers[i].applikationuuid == this.selectApp) {
                    allfiltereddevices.push(this.allregisteredusers[i])
                }

            }
            this.alldevices = allfiltereddevices;

        }
    }
    toogleWeb(e) {
        e.preventDefault();
        // this.loginForm.value.android = this.loginForm.value.android === true ? false : true;
        let v = this.web === true ? false : true;
        this.web = v;


    }
    sendNotifications() {

        console.log(this.titlemodel);
        console.log(this.messagemodel);
        for (let i = 0; i < this.alldevices.length; i++) {
            this.baserestService.sendpushnotitficationsIos(this.alldevices[i].brugernavn,this.titlemodel, this.messagemodel, this.alldevices[i].platform, this.alldevices[i].fcmtoken, this.alldevices[i].applikationuuid, this.sentstatus);
        }

    }
    sentstatus(brugernavn,data,self) {
        console.log("in send noti compo");
        console.log(data);
        if(data.failure ===1){
            console.log( 'sending notification failed to  '+brugernavn);
            self.errorNotification.open();
            self.errorNotification.host['context'].innerText = 'sending notification failed to  '+brugernavn;
        }
        else{
            console.log( 'success notification sent to '+brugernavn);
            self.successNotification.open();
            self.successNotification.host['context'].innerText = 'sucsess notification sent to  '+brugernavn;
        }
    }
}
