import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'message-list',
  templateUrl: 'message-list.html'
})
export class MessageList {

  @Input('message')
  private messages: Message[];

  constructor() { }

  ngOnInit() {
  }

}