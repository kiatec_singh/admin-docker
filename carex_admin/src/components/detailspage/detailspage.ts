import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { Services } from '../../providers/services/services';
import { ActivatedRoute } from '@angular/router';
import { DetailsData, Structure } from '../../models/details.data';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Localization } from '../../models/localization/localization';
import { DawaAutocompleteItem } from 'ngx-dawa-autocomplete';
import { AuthRestService } from '../../providers/auth.rest.service';
import * as moment from 'moment';

declare var jquery: any;
declare var $: any;
declare var globalize: any;
declare var globalization: any;

declare var window: any;



@Component({
    selector: 'detailspage',
    templateUrl: 'detailspage.html'
})
export class DetailsPage {
    public uuid;
    //public detailsData: DetailsData[];
    public detailsData: any;
    public structue: Structure;
    public bi_objekter: any;
    public tabsarray;
    public selectedTab = 0;
    public selectedContainer = 0;
    public detailsForm: FormGroup;
    public items: any;
    private formControl: FormControl;
    private formArray: FormArray;
    private currentTab;
    private currentFormElement;
    private currentItem;
    public loading = false;
    public tabIndex;
    public dropdowns: any;
    public dropdown: any;
    public tabstoDisplay: any = [];
    private listId: any;
    private objecttype: any;
    private localization: Localization;
    private brugervendtnoegle: any;
    private selectedMenu;
    private heading;
    public itemss: DawaAutocompleteItem[] = [];
    public addresserrorMesg = false;
    public enableaddresserrorMsg = false;
    public highlightedIndex: number = 0;
    public selectedStreet: string = '';
    private displayheadingName;
    private lastupdatedTime;
    private brugerName;
    private lastupdatedTimeStamp;
    private errorMessage;
    private File;
    private downloadDetails;
    private fileNavn;
    private documentuuid;
    private previewImage;
    private environmentUrl;
    private imageUrl;
    private AllFiles = [];
    private navigationElements;
    private readonly = false;
    private autorisation: any;
    private detailsform: any;


    @ViewChild('successNotification', { static: false }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: false }) errorNotification: jqxNotificationComponent;

    @ViewChild('detailForm', { static: false }) detailForm;
    @Output() selectitem = new EventEmitter();

    constructor(private service: BaseRestService,
        private services: Services, private activatedRoute: ActivatedRoute, private auth: AuthRestService, private formBuilder: FormBuilder, private location: Location) {
        this.auth.autorisation.subscribe(
            (autorisation: any) => {
                {
                    this.autorisation = autorisation;
                }
            }
        );
    }

    ngOnInit() {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu();//this.activatedRoute.snapshot.data.title;   
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new Localization();
        this.environmentUrl = this.service.getEnvironment();
        this.localization = this.localization.localizationobj;
        this.getDetailedContent(this.uuid, this.listId);
        this.imageUrl = this.environmentUrl.substring(0, this.environmentUrl.indexOf('/api'));
    }
    getDetailedContent(uuid, listId) {
        this.service.getDetailsdata(uuid, listId, this.autorisation)
            .then(
                detailsData => { this.detailsData = detailsData; this.setData() },
                error => (console.log("try again"))
            );
    }

    getHeaderInfo() {
        let details: any = this.detailsData;
        console.log(this);
        this.displayheadingName = details.status.displayname;
        this.services.setdisplayName(details.status.displayname);
        this.brugerName = details.status.brugernavn;
        this.readonly = details.status.readonly ? details.status.readonly : false;
        if (details.status.update_timestamp) {
            this.lastupdatedTime = new Date(details.status.update_timestamp);
            var dd: any = this.lastupdatedTime.getDate();
            var mm: any = this.lastupdatedTime.getMonth() + 1;
            var yyyy = this.lastupdatedTime.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            //dateformat = dd + '-' + mm + '-' + yyyy;
            this.lastupdatedTime = dd + '-' + mm + '-' + yyyy;
        }
        this.constrcutBreadcrum();
    }
    constrcutBreadcrum() {
        if (!this.activatedRoute.snapshot.data.issubmenu) {
            console.log("submenu false");
            this.services.addNavigation(this.displayheadingName);
        }
        if (this.activatedRoute.snapshot.data.issubmenu) {
            console.log("submenu false");
            this.services.setselectedMenu(this.brugervendtnoegle);
            this.services.addNavigation(this.displayheadingName);
        }
        this.navigationElements = this.services.getNavigation();
        console.log(this.navigationElements);
    }
    setData() {
        let details: any = this.detailsData;
        this.getHeaderInfo();
        this.lastupdatedTimeStamp = details.status.update_timestamp;
        this.detailsForm = this.formBuilder.group({
            items: this.formBuilder.array([])
        });
        this.addItem();
    }
    addItem(): void {
        this.items = this.detailsForm.get('items') as FormArray;
        for (let i in this.detailsData.result.detaljer) {
            this.items.push(this.createDyanicForm(this.detailsData.result.detaljer[i]));
        }
        setTimeout(() => {
            $('.selectpicker').selectpicker();
        }, 100);
        this.loading = false;
        console.log(this.detailsForm);
        this.detailsform = this.detailsForm
    }
    createDyanicForm(item): FormGroup {
        var tooltipinfo = this.detailsData.beskrivelser[item.ld_brugervendtnoegle];
        if (item.type == "input" && item.ld_brugervendtnoegle != "adresser" && item.type != "file" && item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values]

            });
        }
        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values]
            });
        }
        if (item.type == "file") {
            this.selectedStreet = item.selected_values[0];
            this.documentuuid = item.dokument_uuid;
            let image = item.image ? item.image : null;
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                documentuuid: item.dokument_uuid,
                image: image,
                selected_list: [item.selected_values]
            });
        }
        if (item.type == "date") {
            var mydate = null;
            if (item.selected_values && item.selected_values[0]) {
                mydate = new Date(item.selected_values[0]);
                var dd: any = mydate.getDate();
                var mm: any = mydate.getMonth() + 1;
                var yyyy = mydate.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                mydate = dd + '-' + mm + '-' + yyyy;
                mydate = moment(mydate, "DD-MM-YYYY");

            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: mydate,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values[0]],
                formatdate: mydate ? mydate : null
            });
        }

        if (item.type == "dropdown") {
            let source = [];
            let selected_names = []
            for (var i in item.valuelist) {
                let isslctd = 'false';
                if (item.selected_values && item.selected_values.length != 0) {
                    isslctd = item.selected_values.indexOf(i) != -1 ? 'true' : 'false';
                    if (isslctd === 'true') { selected_names.push(item.valuelist[i]); }
                }
                source.push({
                    key: i,
                    value: item.valuelist[i],
                    selected: isslctd
                })
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                options: [source],
                multiselect: item.multiselect,
                tooltip: tooltipinfo,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                valuelist: item.valuelist,
                selected_list: [item.selected_values],
                selected_names: [selected_names],
                //selected_names: selected_names && selected_names.length ? [selected_names]:null,
                selected_values: [item.selected_values]
            });
        }
    }

    downloadFile(event) {
        event.preventDefault();
        // detailsData => { this.detailsData = detailsData; this.setData() },
        this.service.downloadFileDocument(this.listId, this.documentuuid).then(
            res => { this.downloadDetails = res; this.getFile(res); this.successNotification.open(); this.loading = false; },
            error => { this.errorNotification.open(); this.errorMessage = error.status.besked; console.log(error); this.loading = false; }
        );
    }
    getFile(result) {
        let temp_Url = result.result.temp_path;
        let envi = this.service.getEnvironment();
        let e = envi.indexOf('/api');
        let formurl = envi.substring(0, e);
        let tempindex = temp_Url.lastIndexOf('temp');
        let filedownload_url = temp_Url.substring(tempindex, temp_Url.length);
        let downloadURL = formurl.concat("/" + filedownload_url);
        console.log(downloadURL);
        // window.open(downloadURL);
        console.log(window);
        window.open('' + downloadURL, '_blank');
        //let openWindow:any = this.windowrefService.nativeWindow();
        // openWindow.open(''+downloadURL, '_blank');
        //openWindow.location.replace(''+downloadURL);
    }

    Select(event, itemname): void {
        let slctItem = event.target.value;
        let slctdropdownItem = itemname.value;
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slctdropdownItem.ld_brugervendtnoegle)[0];
        for (let j in filtereditem.options) {
            if (slctdropdownItem.selected_names[0] === filtereditem.options[j].value) {
                filtereditem.options[j].selected = "true";
            }
            else {
                filtereditem.options[j].selected = "false";
            }
        }
         this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slctdropdownItem.ld_brugervendtnoegle)[0]= filtereditem;
        $('.invalid-feedback.'+slctdropdownItem.ld_brugervendtnoegle).addClass('hide');
    }
    Multiselect(event, itemname): void {
        let slctItem = event.target.value;
        let slctdropdownItem = itemname.value;
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slctdropdownItem.ld_brugervendtnoegle)[0];
        for (let j in filtereditem.options) {
            if (slctdropdownItem.selected_names.indexOf(filtereditem.options[j].value) != -1) {
                filtereditem.options[j].selected = "true";
            }
            else {
                filtereditem.options[j].selected = "false";
            }
        }
         this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slctdropdownItem.ld_brugervendtnoegle)[0]= filtereditem;
        if(slctdropdownItem.required ==='true'){
            if(slctdropdownItem.selected_names && slctdropdownItem.selected_names.length!=0){
                event.path[1].classList.add('ng-valid');
                event.path[1].classList.remove('ng-invalid');
            }
            else{
                event.path[1].classList.remove('ng-valid');
                event.path[1].classList.add('ng-invalid');
            }
        }
        $('.invalid-feedback.' + itemname.ld_brugervendtnoegle).addClass('hide');
}

ErrorMsg() {
    if (this.itemss.length > 0) {
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    }
    if (this.selectedStreet && this.selectedStreet.length > 0) {
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    }
    else {
        this.addresserrorMesg = true;
        this.enableaddresserrorMsg = true;
    }
}
    public onItems(items) {
    this.itemss = items;

}

    public onItemHighlighted(index) {
    this.highlightedIndex = index;
    if (index == 0) {
        this.addresserrorMesg = true;
        this.enableaddresserrorMsg = true;
    }
}

    public onItemSelected(item) {
    this.itemss = [];
    this.highlightedIndex = 0;
    //this.selectedStreet = item.fullStreet;
    this.selectedStreet = item.text;
    // this.ref.detectChanges();
    this.addresserrorMesg = false;
    this.enableaddresserrorMsg = false;
}

Cancel(event): void {
    event.preventDefault();
    this.location.back();
}
Delete(event) {
    event.preventDefault();
    if (confirm("Are you sure you want to delete this entry of ID::" + this.uuid) == true) {
        this.service.deleteDetailsdata(this.listId, this.uuid, this.autorisation, this.lastupdatedTimeStamp).then(
            res => {
                this.location.back();
                this.successNotification.open(); this.loading = false;
            },
            error => { this.errorNotification.open(); this.errorMessage = error.status.besked;; this.loading = false; },
            complete => { this.loading = false; }
        );
    } else {
        console.log("You pressed Cancel!");
    }

}

saveData(event) {
    event.preventDefault();
    if (!this.detailForm.nativeElement.checkValidity()) {
        this.detailForm.nativeElement.classList.add('was-validated');
        //event.stopPropagation();
    }
    let formvalid = $('.invalid-feedback').is(':visible');
    console.log(formvalid + '--submited');
    if (!formvalid) {
        var formvalues = JSON.stringify(this.detailsForm.controls.items.value);
        let formdata: any = [];
        formdata.push(this.detailsForm.controls.items.value.map(this.formatData, this));
       // console.log(JSON.stringify(formdata[0]));
        this.loading = true;
        this.service.updateDetailsdata(this.listId, this.uuid, this.autorisation, this.detailsData.status.update_timestamp, JSON.stringify(formdata[0]), this.AllFiles).then(
            res => {
                this.location.back();
                this.successNotification.open(); this.loading = false;
            },
            error => {
                this.errorNotification.open();
                console.log(error.error.status.besked);
                let error_messsage = error.error.status.besked;
                this.errorMessage = error_messsage;
                this.loading = false;
            },
            complete => { this.loading = false; }
        );
    }

}
selectFile(event, item) {
    console.log(item);
    item.value.value = event.target.files[0].name;
    this.fileNavn = event.target.files[0].name;
    this.File = event.target.files[0];
    let fileitem = {
        name: item.value.ld_brugervendtnoegle,
        value: this.File
    }
    this.AllFiles.push(fileitem);
    // let selected_index = event.args.item.index;
    // let selected_value = event.args.item.value;
    let selected_item = item.value.name;
    var item = this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0];
    item.value = this.fileNavn;
    this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0] = item;
}


formatData(item) {
    if (item) {
        if (item.type == "input" && item.ld_brugervendtnoegle != "adresser") {
            return {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: [item.value],
                relationindeks: item.relationindeks
            }
        }
        if (item.type == "date") {
            let formateddate = null;
            if (item.value && item.formatdate._d) {
                var dd: any = item.formatdate._d.getDate();
                var mm: any = item.formatdate._d.getMonth() + 1;
                var yyyy = item.formatdate._d.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                formateddate = yyyy + '-' + mm + '-' + dd;
            }
            return {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: formateddate ? [formateddate] : [item.value],
                relationindeks: item.relationindeks
            }
        }
        if (item.type == "file") {
            let image = item.image ? item.image : null;
            return {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: [item.value],
                relationindeks: item.relationindeks,
                image: image
            }
        }

        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            return {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: [this.selectedStreet],
                relationindeks: item.relationindeks
            }
        }

        if (item.type == "dropdown" && item.multiselect == "false") {
            var optionitem = item.options.filter(x => x.selected === "true")[0];
            if (optionitem) {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [optionitem.key],
                    relationindeks: item.relationindeks
                }
            }
            else {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [null],
                    relationindeks: item.relationindeks
                }
            }
        }

        if (item.type == "dropdown" && item.multiselect == "true") {
            var values = [];
            if (item.selected_names && item.selected_names.length != 0) {
                for (let i in item.options) {
                    if (item.selected_names.indexOf(item.options[i].value) != -1) {
                        values.push(item.options[i].key);
                    }
                }
            }
            return {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: values,
                relationindeks: item.relationindeks
            }
        }
    }

}

dateChange(item, index) {
    let dateformat = null;
    console.log(item);
    if (index && index.selected) {
        var selecteddate: any = new Date(index.selected._d);
        var dd: any = selecteddate.getDate();
        var mm: any = selecteddate.getMonth() + 1;
        var yyyy = selecteddate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        dateformat = dd + '-' + mm + '-' + yyyy;
        dateformat = moment(dateformat, "DD-MM-YYYY");
        }
        let slcitem = item.value.ld_brugervendtnoegle;
        item.value.value = dateformat;
        item.value.formatdate = dateformat;
    this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
}
clearDate(item){
    item.value.value = null;
    item.value.formatdate = null;
    let slcitem = item.value.ld_brugervendtnoegle;
    this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
}
}