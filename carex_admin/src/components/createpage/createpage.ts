import { Component, OnInit, ViewChildren, ViewEncapsulation, AfterContentChecked, QueryList, ViewChild, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BaseRestService } from '../../providers/base.rest.service';
import { Services } from '../../providers/services/services';
import { ActivatedRoute } from '@angular/router';
import { DetailsData, Structure } from '../../models/details.data';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { Options } from '../../models/dropdowns';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { DawaAutocompleteItem } from 'ngx-dawa-autocomplete';
//import { globalization} from 'jqwidgets-scripts/jqwidgets/globalization/globalize.culture.da-Dk';
import { AuthRestService } from '../../providers/auth.rest.service';
import * as moment from 'moment';
declare var jquery: any;
declare var $: any;

@Component({
    selector: 'createpage',
    templateUrl: 'createpage.html'
    //encapsulation: ViewEncapsulation.None
})
export class CreatePage {
    public uuid: string;
    public detailsData: DetailsData[];
    public structue: Structure;
    public bi_objekter: any;
    public tabsarray;
    public selectedTab = 0;
    public selectedContainer = 0;
    public detailsForm: FormGroup;
    public items: any;
    private formControl: FormControl;
    private formArray: FormArray;
    private currentTab;
    private currentFormElement;
    private currentItem;
    public loading = false;
    public tabIndex;
    public dropdowns: any;
    public dropdown: any;
    public option: Options;
    public tabstoDisplay: any = [];
    public tabsKeys: any = [];
    private listId: any;
    private objecttype: any;
    //private localization: Localization;
    public itemss: DawaAutocompleteItem[] = [];
    public addresserrorMesg = false;
    public enableaddresserrorMsg = false;
    public highlightedIndex: number = 0;
    public selectedStreet: string = '';
    public selectedMenu: any;
    private displayheadingName;
    private lastupdatedTime;
    private brugerName;
    private heading;
    private fileNavn;
    private File;
    private AllFiles = [];


    @ViewChild('detailForm', { static: false }) detailForm;
    @ViewChild('successNotification', { static: false }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: false }) errorNotification: jqxNotificationComponent;

    @Output() selectitem = new EventEmitter();
    public autorisation: any;

    // 
    constructor(private service: BaseRestService, private auth: AuthRestService, private services: Services, private route: ActivatedRoute, private formBuilder: FormBuilder, private location: Location, private datepipe: DatePipe) {
        //this.createForm();
        this.auth.autorisation.subscribe(
            (autorisation: any) => {
                {
                    this.autorisation = autorisation;
                }
            }
        );
    }

    ngOnInit() {
        this.loading = true;
        //this.uuid = this.route.snapshot.params.uuid;
        //this.listId = this.route.snapshot.params.listId;
        //this.objecttype = this.route.snapshot.params.objecttype;
        // this.localization = new Localization();
        // this.localization = this.localization.localizationobj;
        this.listId = this.services.getlistId();
        this.selectedMenu = this.location;
        this.heading = "Opret";
        var selectedURL = this.selectedMenu._platformStrategy._platformLocation.pathname.substring(1);
        var fields = selectedURL.split('/');

        this.selectedMenu = fields[0];
        this.getDetailedContent(this.autorisation, this.listId);
    }

    getDetailedContent(autorisation, listId) {
        this.service.getcreateDetailsdata(listId, this.autorisation)
            .then(
                detailsData => { this.detailsData = detailsData; this.setData() },
                error => (console.log("try again"))
            );
    }

    ErrorMsg() {
        if (this.itemss.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        if (this.selectedStreet && this.selectedStreet.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        else {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    }
    public onItems(items) {
        this.itemss = items;

    }

    public onItemHighlighted(index) {
        this.highlightedIndex = index;
        if (index == 0) {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    }

    public onItemSelected(item) {
        this.itemss = [];
        this.highlightedIndex = 0;
        //this.selectedStreet = item.fullStreet;
        this.selectedStreet = item.text;
        // this.ref.detectChanges();
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    }
    getHeaderInfo() {
        let details: any = this.detailsData;
        this.displayheadingName = details.status.displayname;
        this.brugerName = details.status.brugernavn;

        if (details.status.update_timestamp) {
            this.lastupdatedTime = new Date(details.status.update_timestamp);
            var dd: any = this.lastupdatedTime.getDate();
            var mm: any = this.lastupdatedTime.getMonth() + 1;
            var yyyy = this.lastupdatedTime.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }
            //dateformat = mm + '-' + dd + '-' + yyyy;
            this.lastupdatedTime = yyyy + '-' + mm + '-' + dd;
        }
    }

    setData() {
        let details: any = this.detailsData;
        this.getHeaderInfo();

        for (let item in details.result) {
            let currentitem: any = item;
            console.log(item);
            this.tabstoDisplay.push(item);
            this.tabsKeys.push(item);

        }


        //this.structue = new Structure(details.result.struktur);
        //this.bi_objekter = new DetailsData(details.result.bi_objekter);
        this.createForm();

    }

    createForm() {
        this.loading = false;
        this.detailsForm = this.formBuilder.group({
            items: this.formBuilder.array([])
        });
        this.addItem();
        setTimeout(() => {
            $('select').selectpicker('val', '');
            this.loading = false;
        }, 100);
    }
    createItem(): FormGroup {
        return this.formBuilder.group({
        });

    }
    addItem(): void {
        this.items = this.detailsForm.get('items') as FormArray;
        var tabs: any = this.tabstoDisplay;
        var tabItems: any = this.detailsData;
        for (var i = 0; i < tabs.length; i++) {
            this.tabIndex = i;
            this.currentTab = this.tabstoDisplay[i];
            for (var tabitem in tabItems.result[this.currentTab]) {
                this.items.push(this.createDyanicForm(tabItems.result[this.currentTab], tabitem));
            }

        }
    }


    createDyanicForm(currenttab, tabitem): FormGroup {
        var formitem: any[];
        //var itemname = this.currentItem.titel;
        //var itemvalue = this.currentItem.value;
        //var postnameparamater = this.currentItem.postname;
        let tooltipdata: any = this.detailsData;


        var item = currenttab[tabitem];
        var tooltipinfo = tooltipdata.beskrivelser[item.ld_brugervendtnoegle];
        if (item.type == "input" && item.ld_brugervendtnoegle != "adresser" && item.type != "file" && item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values]

            });
        }
        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                readonly: item.readonly,
                tooltip: tooltipinfo,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values]


            });
        }
        if (item.type == "file") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                readonly: item.readonly,
                tooltip: tooltipinfo,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values]


            });
        }
        if (item.type == "date") {
            var mydate = null;
            if (item.selected_values && item.selected_values[0]) {
                mydate = new Date(item.selected_values[0]);
                var dd: any = mydate.getDate();
                var mm: any = mydate.getMonth() + 1;
                var yyyy = mydate.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                mydate = dd + '-' + mm + '-' + yyyy;
                mydate = moment(mydate, "DD-MM-YYYY");

            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: mydate,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                formatdate: mydate ? mydate : null
            });
        }
        if (item.type == "dropdown") {
            let source = [];
            var index;
            var itemindex;
            for (var i in item.valuelist) {
                let isslctd = 'false';
                if (item.selected_values && item.selected_values.length != 0) {
                    isslctd = item.selected_values.indexOf(i) != -1 ? 'true' : 'false';
                }
                source.push({
                    key: i,
                    value: item.valuelist[i],
                    selected: isslctd
                })
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                options: [source],
                selected_index: index,
                multiselect: item.multiselect,
                tooltip: tooltipinfo,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                valuelist: item.valuelist,
                selected_values: item.selected_values
            });
        }
    }
    createFormDataElement(item: any): FormGroup {
        var title = item.titel;
        var value = item.value;
        this.loading = false;
        for (let i in item) {
            return new FormGroup({
                item: new FormControl()
            })
        }
    }


    switchTab(event, id): void {
        event.preventDefault();
        this.selectedTab = id;
        this.selectedContainer = id;
    }

    Cancel(event): void {
        event.preventDefault();
        this.location.back();
    }
    Delete() {
        confirm("Are you sure you want to delete this entry of ID::" + this.uuid);
    }

    saveData(event) {
        event.preventDefault();
        if (!this.detailForm.nativeElement.checkValidity()) {
            this.detailForm.nativeElement.classList.add('was-validated');
            //event.stopPropagation();
        }
        let formvalid = $('.invalid-feedback').is(':visible');
        let invalid = $('.ng-invalid');
        //if (!formvalid && !invalid) {
            if (!formvalid) {
            var formvalues = JSON.stringify(this.detailsForm.controls.items.value);
            let formdata: any = [];
            formdata.push(this.detailsForm.controls.items.value.map(this.formatData, this));
            let lastupdatetime: any = this.detailsData;
            formdata["updatedfields"]=this.detailsForm.controls.items.value.map(this.formatData, this);      
            formdata.push({update_timestamp: lastupdatetime.status.update_timestamp});
            this.loading = true;
            this.service.createDetailsdata(this.listId, this.uuid, this.autorisation, lastupdatetime.status.update_timestamp, JSON.stringify(formdata[0]), this.AllFiles).then(
                res => { this.successNotification.open(); this.location.back(); },
                error => { this.errorNotification.open(); this.loading = false; },
                complete => { this.loading = false; }
            );
        }
    }
    selectFile(event, item) {
        console.log(item);
        item.value.value = event.target.files[0];
        this.fileNavn = event.target.files[0].name;
        this.File = event.target.files[0];

        let fileitem = {
            name: item.value.ld_brugervendtnoegle,
            value: this.File
        }
        this.AllFiles.push(fileitem);
        let selected_item = item.value.name;
        var item = this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0];
        item.value = this.fileNavn;
        this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0] = item;

    }

    Select(event, itemname): void {
        let slctItem = event.target.value;
        for (let i in this.detailsForm.controls.items.value) {
            if (this.detailsForm.controls.items.value[i].ld_brugervendtnoegle === itemname.ld_brugervendtnoegle) {
                for (let j in this.detailsForm.controls.items.value[i].options) {
                    if (this.detailsForm.controls.items.value[i].options[j].value === slctItem) {
                        this.detailsForm.controls.items.value[i].options[j].selected = "true";
                    }
                    else {
                        this.detailsForm.controls.items.value[i].options[j].selected = "false";
                    }
                }
            }
        }
       $('.invalid-feedback.' + itemname.ld_brugervendtnoegle).addClass('hide');
    }

    Multiselect(event, itemname, multiselect): void {
        let slctItem = event.target.value;
        console.log(multiselect);
        for (let i in this.detailsForm.controls.items.value) {
            if (this.detailsForm.controls.items.value[i].ld_brugervendtnoegle === itemname.ld_brugervendtnoegle) {
                for (let j in this.detailsForm.controls.items.value[i].options) {
                    if (itemname.selected_list.indexOf(this.detailsForm.controls.items.value[i].options[j].value) != -1) {
                        this.detailsForm.controls.items.value[i].options[j].selected = "true";
                    }
                    else {
                        this.detailsForm.controls.items.value[i].options[j].selected = "false";
                    }
                }
            }
        }
        if(itemname.required ==='true'){
            if(itemname.selected_list && itemname.selected_list.length!=0){
                event.path[1].classList.add('ng-valid');
                event.path[1].classList.remove('ng-invalid');
            }
            else{
                event.path[1].classList.remove('ng-valid');
                event.path[1].classList.add('ng-invalid');
            }
        }
        $('.invalid-feedback.' + itemname.ld_brugervendtnoegle).addClass('hide');
    }

    formatData(item) {
        if (item) {
            if (item.type == "input" && item.ld_brugervendtnoegle != "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                    relationindeks: item.relationindeks
                }
            }
            if (item.type == "date") {
                let formateddate = null;
                if (item.value && item.formatdate._d) {
                    var dd: any = item.formatdate._d.getDate();
                    var mm: any = item.formatdate._d.getMonth() + 1;
                    var yyyy = item.formatdate._d.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    formateddate = yyyy+ '-' + mm + '-' + dd;
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: formateddate ? [formateddate] : [item.value],
                    relationindeks: item.relationindeks
                }
            }
            if (item.type == "file") {
                let image = item.image ? item.image : null;
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                    relationindeks: item.relationindeks,
                    image: image
                }
            }

            if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [this.selectedStreet],
                    relationindeks: item.relationindeks
                }
            }

            if (item.type == "dropdown" && item.multiselect == "false") {
                var optionitem = item.options.filter(x => x.selected === "true")[0];
                if (optionitem) {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [optionitem.key],
                        relationindeks: item.relationindeks
                    }
                }
                else {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [null],
                        relationindeks: item.relationindeks
                    }
                }
            }

            if (item.type == "dropdown" && item.multiselect == "true") {
                var values = [];
                if (item.selected_list && item.selected_list.length != 0) {
                    for (let i in item.options) {
                        if (item.selected_list.indexOf(item.options[i].value) != -1) {
                            values.push(item.options[i].key);
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values,
                    relationindeks: item.relationindeks
                }
            }
        }

    }
    dateChange(item, index) {
        console.log(item);
        let dateformat:any;
        if(index && index.selected){
        var selecteddate: any = new Date(index.selected._d);
        var dd: any = selecteddate.getDate();
        var mm: any = selecteddate.getMonth() + 1;
        var yyyy = selecteddate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        dateformat = dd + '-' + mm + '-' + yyyy;
        dateformat = moment(dateformat, "DD-MM-YYYY");
        }
        let slcitem = item.value.ld_brugervendtnoegle;
        item.value.value = dateformat;
        item.value.formatdate = dateformat;
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
    }
    clearDate(item){
        item.value.value = null;
        item.value.formatdate = null;
        let slcitem = item.value.ld_brugervendtnoegle;
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
    }

}