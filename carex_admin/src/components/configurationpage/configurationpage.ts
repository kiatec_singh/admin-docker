import {Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {data} from '../../models/data/data';


declare var jquery: any;
declare var $: any;

@Component({
    selector:'configurationpage',
    templateUrl: 'configurationpage.html'
})
export class ConfigurationPage {
    private configureForm : FormGroup;
    customerList:any = data ;
    customerpages: Array<{title: string, component: any}>;

    private customerName;
    constructor() {
        //this.createForm();
        this.configureForm = new FormGroup({
            selectCustomer : new FormControl()
        })
     
    }

    ngOnInit() {
        console.log("in config");
        let pagesarray=[]; 
 
        for(let i in this.customerList){
          pagesarray.push({
           title: this.customerList[i].organisation, 
           pageData:this.customerList[i],
           index: i 
          });''
        }
        this.customerpages = pagesarray;
    }
    selectedCustomer(){
        console.log(this.customerName);
        $('button.bar-button-menutoggle').click()


    }
}