import { Component, Output, Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import exportFromJSON from 'export-from-json';

declare var require: any;
declare var $: any;
var xlsx = require('json-as-xlsx');


@Component({
    selector: 'serviceanalytics-viewer',
    templateUrl: 'serviceanalytics.html'
})
export class serviceanalyticsComponent {

    private alldomainsdata: any;
    private serviceanalyticsForm: any;
    private filteredata: any;
    private startdate = null;
    private enddate = null;
    

    constructor(private fb: FormBuilder, private router: Router, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.serviceanalyticsForm = this.fb.group({
            application: ['', Validators.required],
            startdate: [''],
            enddate: ['']
        });
    }

    ngOnInit() {

    }
    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    handleChange(name) {
        console.log(name);
    }
    getanalyticsData() {
        console.log("oin submit");
    }
    StartDate(i, e) {
        //this.getfiltereddata();
    }
    EndDate(i, e) {
        console.log(e);
    }
    clearstartDate(item) {
        console.log(item);
        this.startdate = null;
    }
    clearendDate(item) {
        this.enddate = null;
    }

    setData(){

    }

    downloadservicelogs(e) {
        let startdate = null;
        let enddate = null;
        if (this.startdate && this.startdate._d) {
            var dd: any = this.startdate._d.getDate();
            var mm: any = this.startdate._d.getMonth() + 1;
            var yyyy = this.startdate._d.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            startdate = yyyy + '-' + mm + '-' + dd;
        }
        if (this.enddate && this.enddate._d) {
            var dd: any = this.enddate._d.getDate();
            var mm: any = this.enddate._d.getMonth() + 1;
            var yyyy = this.enddate._d.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            enddate = yyyy + '-' + mm + '-' + dd;
        }
        this.baserestService.getserviceanalytics(startdate,enddate).then(
            (servicelogsdata) => {
                console.log(servicelogsdata);
                this.filteredata = servicelogsdata;
                this.download();
            }
        )
    }

    download() {
        var excelcolumns = [];
        var data=[];
        const exceldata = [{ foo: 'foo'}, { bar: 'bar' }]
       // const data = [{ foo: 'foo'}, { bar: 'bar' }]
        this.filteredata.map(function (item) {
            let timestamp = item.log_timestamp;
            let requestdata = item.request;
            if(item.request){
                requestdata.log_timestamp = timestamp;
            data.push(requestdata);
            for(let i in requestdata){
                requestdata[i];
                let element;
                element= excelcolumns.filter(x => x.label === i);
                if(!element.length){
                    excelcolumns.push({ label: i, value:row => ((row.i!=null || row.i!=undefined)? i : '')}); 
                }
            }
        }
        });
        // excelcolumns.push({ label: 'action', value: 'action' });
        // // excelcolumns.push({ label: 'name', value: 'name' });
        // excelcolumns.push({ label: 'platform', value: 'platform' });
        // excelcolumns.push({ label: 'autorisation', value: 'autorisation' });
        // excelcolumns.push({ label: 'bruger_uuid', value: 'bruger_uuid' });
        // excelcolumns.push({ label: 'eventname', value: 'eventname' });
        // excelcolumns.push({ label: 'itsystem_uuid', value: 'itsystem_uuid' });
        // excelcolumns.push({ label: 'applikation_uuid', value: 'applikation_uuid' });
        // excelcolumns.push({ label: 'version', value: 'version' });
        // excelcolumns.push({ label: 'log_timestamp', value: 'log_timestamp' });
        var settings = {
            sheetName: "ServicelogAnalytics",
            fileName: "Servicelogs",
        }
       // const data = [{ foo: 'foo'}, { bar: 'bar' }]
const fileName = 'download'
const exportType = 'xls'
      //  xlsx(excelcolumns, exceldata, settings);
      exportFromJSON({ data, fileName, exportType });
    }
}
