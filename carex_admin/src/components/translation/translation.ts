import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';


declare var require: any;
declare var $: any;
var xlsx = require('json-as-xlsx');

@Component({
    selector: 'translation',
    templateUrl: 'translation.html'
})

export class Translation {
    private userrelations: any;
    private modalReference: any;
    private Objekttype: any = "skema";
    private slctorg: any = "";
    private authorisation: any;
    private loading = false;

    @ViewChild('successNotification',{static:false}) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification',{static:false}) errorNotification: jqxNotificationComponent;
    @ViewChild('infoNotification',{static:false}) infoNotification: jqxNotificationComponent;



    private localizationObject: any;
    public uploadStatus = false;
    public fileStatus = false;
    private selectedMenu;
    private uploadContainer = false;
    private fileName;
    private uploadedFile;
    private serviceErrorMessage;

    constructor(private baserestService: BaseRestService, private modalService: NgbModal, private changeDetector: ChangeDetectorRef, private auth: AuthRestService) {
        this.auth.autorisation.subscribe(
            (authorisation) => {
                {
                    this.authorisation = authorisation;
                }
            }
        );
        this.auth.userrelations.subscribe(
            (userrelations) => {
                {
                    this.userrelations = userrelations;
                }
            }
        );
    }

    ngOnInit() {
        console.log("in Translation")
        console.log(this.userrelations);
    }
    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    open(modal, event) {
        this.changeDetector.detectChanges();
        event.preventDefault();
        this.changeDetector.detectChanges();
        this.modalReference = this.modalService.open(modal);
        this.modalReference.result.then((result) => {
            //this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    onChange(e, value) {
        console.log(value);
        console.log(e.target.value);
        setTimeout(() => {
           $('select').selectpicker(); 
        }, 10);
    }
    export(modal, event) {
        this.changeDetector.detectChanges();
        event.preventDefault();
        console.log(this);
        this.loading = true;
        this.baserestService.translateExport(this.authorisation, this.Objekttype, this.slctorg).then(
            res => { console.log(res); this.loading = false; this.downloaData(res); },
            error => { console.log(error); this.loading = false; }
        );
    }
    downloaData(res) {
        if (res && res.data && res.data.length != 0) {
            var orgname;
            this.userrelations.map(function (org) {
                if (this.slctorg == org.organisation_uuid) {
                    orgname = org.org;
                    return;
                }
            }, this);
            const data = res.data;//? res.data : [{ Nodata: 'No data found..' }];
            const fileName = orgname + '_' + this.Objekttype;
            const exportType = 'xls';
            //  exportFromJSON({ data, fileName, exportType });
            var columns = [];
            res.overskrifter.map(function (item) {
                columns.push({ label: item, value: item });
            });
            var settings = {
                sheetName: this.Objekttype,
                fileName: orgname
            }
            if(res.data.length===0){
                res.data.push(['']);
            }
            xlsx(columns, res.data, settings);
            this.successNotification.open();
        }
        else {
            this.infoNotification.open();
        }
    }

    onfileChange(event) {
        event.preventDefault();
        this.uploadContainer = true;
        this.fileName = event.target.files[0];

    }
    onfileSubmit(content, event: any) {
        console.log(content);
        this.loading = true;
        let filetype;
        if (this.fileName && this.fileName.type) {
            filetype = this.fileName.type;
        }
        else {
            if (this.fileName && this.fileName[0] && this.fileName[0].type) {
                filetype = this.fileName[0].type;
            }
            else {
                filetype = this.fileName.name.split('.').pop();
            }
        }

        let uplaodedfile = this.fileName[0] ? this.fileName[0] : this.fileName;
        let filename = this.fileName.name;
        if (filetype == "text/csv" || filetype == "csv" || filetype == "application/pdf"
            || filetype == "application/json" || filetype == "json" || filetype == "xlsx" || filetype == "application/vnd.ms-excel"
            || filetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            console.log("in submit");
            this.loading = true;
            this.baserestService.translateImport(uplaodedfile, filename, this.authorisation, this.Objekttype, this.slctorg).then(
                res => {
                    console.log(res);
                    //if (res.status.kode != 9) {
                    if (res && res.response && res.response.success) {
                        this.serviceErrorMessage = res.response.success;
                        this.successNotification.open();
                        this.uploadStatus = false;
                        this.fileName = null;
                        this.uploadContainer = false;
                        this.loading = false;
                        this.modalReference.close();
                    }
                    else {
                        //$('.errormessage').text(res.status.besked);
                        this.serviceErrorMessage = res && res.status && res.status.besked ? res.status.besked : "Error";
                        this.errorNotification.open();
                        this.uploadStatus = false;
                        this.fileName = null;
                        this.uploadContainer = false;
                        this.loading = false;
                        this.modalReference.close();
                    }

                },
                error => {
                    this.errorNotification.open();
                    this.uploadStatus = false;
                    this.fileName = null;
                    this.uploadContainer = false;
                    this.loading = false;
                }
            );
        }
        else {
            this.fileStatus = true;
            this.uploadContainer = false;
            console.log("in cancel ");
            //uploadFile.nativeElement[0].files[0] = null
            this.fileName = null;
            this.uploadContainer = false;
            this.loading = false;
            setTimeout(() => {
                this.fileStatus = false;
            }, 4000);
        }

    }
    fileuploadCancel(event) {
        event.preventDefault();
        this.uploadContainer = false;
        console.log("in cancel ");
        this.fileName = null;
        this.uploadContainer = false;

    }
}