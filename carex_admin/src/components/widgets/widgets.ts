import {Component, Input, ViewChild, AfterViewInit} from '@angular/core';
import { Message } from '../../models/message';
import {  jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';


@Component({
    selector:'widgets',
    templateUrl: 'widgets.html'
})

export class Widgets{
@ViewChild('widgetCalendar',{static:false}) widgetCalendar: jqxCalendarComponent;

/* public date:Date;
@Input('message')
private message: Message;

@Input('messages') */
private message: Message;
    constructor(){

    }

/*     ngOnInit(){
    this.date = new Date();
    console.log(this.messages);
    console.log(this.message); */
ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    let date1 = new Date();
        let date2 = new Date();
        let date3 = new Date();
        date1.setDate(5);
        date2.setDate(15);
        date3.setDate(16);
        // Add special dates by invoking the addSpecialDate method.
        this.widgetCalendar.addSpecialDate(date1, '', 'Appointment with Alborg Kommune');
        this.widgetCalendar.addSpecialDate(date2, '', 'Meeting with Diabeticc patient from Arhus');
        this.widgetCalendar.addSpecialDate(date3, '', 'Doctors Unit meeting');
}

    ngOnInit() {
      //  this.message = new Message("hi how are you ", "assets/icons/user.png", new Date);
        console.log(this.message);
    }

}
