import { Component, OnInit, ViewChild, ViewChildren, AfterViewInit, ChangeDetectorRef, ViewContainerRef, ElementRef } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { Services } from '../../providers/services/services';
import { RowsList, GridData, Columns, DataFields, ObjectType } from '../../models/grid.data';
import { Localization } from '../../models/localization/localization';
import {ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { modalComponent } from '../modal/modal';
import { Router, ActivatedRoute } from '@angular/router';
import { UserData } from '../../models/user.model';
import { Form } from '@angular/forms';

declare var require: any;
var xlsx = require('json-as-xlsx');
declare var jquery: any;
declare var $: any;


declare var window: any;


@Component({
    selector: 'itsystems',
    templateUrl: 'itsystems.html'
})

export class Itsystems {

    @ViewChild('successNotification',{static:true}) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification',{static:true}) errorNotification: jqxNotificationComponent;

    @ViewChild('gridReference',{static:false}) itsystemGrid: jqxGridComponent;
    @ViewChild('uploadfileForm',{static:false}) uploadfileForm: Form;
    @ViewChild('inputFile',{static:false}) inputFile;
    @ViewChild('modal',{static:false}) Modal;
    // @ViewChild('modal') content: HTMLDivElement;

    private contentPlaceholder: ElementRef;


    private uuid: string;
    private gridData: GridData;
    private rows: RowsList;
    private columnss: Columns;
    private datafileds: DataFields;
    gridrowsandColumns: GridData[];
    private localization: Localization;
    private objektType: ObjectType;
    private source: any;
    private columns;
    private dataAdapter;
    private jqxGridComponent: jqxGridComponent;
    private settings: any;
    private localizationObject: any;
    public loading = false;
    public UserInfo: UserData;
    public uploadStatus = false;
    public fileStatus = false;
    private selectedMenu;
    private uploadContainer = false;
    private fileName;
    private uploadedFile;
    private modalReference: any;
    private groupableValue;
    private modalComponent: modalComponent;
    private heading;
    private documentuuid;
    private downloadDetails;
    private errorMessage;
    private filepath;
    private clickedrow;
    private parentheading;
    private displayname;
    private environmentUrl;
    private navigationElements;
    private serviceErrorMessage;
    private autorisation;




    constructor(private service: BaseRestService, private services: Services, private router: Router, private location: Location, private activatedRoute: ActivatedRoute, 
        private modalService: NgbModal, private changeDetector: ChangeDetectorRef) {
        //  this.uuid = "b8f186ac-aed5-4c87-8e8f-62afe0539b35" ;//"e4c4c23f-0c51-4ee4-8bc3-f70c27e5b786";

    }

    ngOnInit() {
        this.loading = true;
        this.localization = new Localization();
        this.localization = this.localization.localizationobj;
        var userinfo = $("#userinfo").val();
        //this.location = new Location();
        // console(this.location);
        let userinformation: any = userinfo;
        var obj = JSON.parse(userinformation);
        this.UserInfo = new UserData(obj);
        // this.selectedMenu = this.location;
        // var selectedURL = this.selectedMenu._platformStrategy._platformLocation.pathname.substring(1);
        // var fields = selectedURL.split('/');
        // this.selectedMenu = fields[0];
        // this.uuid = fields[1];
        this.parentheading = this.services.getselectedMenu();
        if (this.parentheading != "Personer") {
            this.parentheading = null;
        }
        //   this.parentheading = this.services.getselectedMenu();
        //   this.displayname = this.services.getdisplayName();
        this.heading = this.activatedRoute.snapshot.data.title;
        this.autorisation = this.activatedRoute.snapshot.data.autorisation;
        //this.services.addNavigation(this.heading);
        this.selectedMenu = this.activatedRoute.snapshot.routeConfig.path;
        this.uuid = this.activatedRoute.snapshot.data.listId;
        this.services.setlistId(this.uuid);
        this.clickedrow = this.services.getClickedRowuuiid();
        this.environmentUrl = this.service.getEnvironment();
        this.getValueFromObservable();
    }

    ngAfterViewInit() {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
    }
    onSelect(event: any): void {
        let args = event.args;
        let fileName = args.file;
        let fileSize = args.size;
        console.log(fileName);
        console.log(fileSize);
        var ext = fileName.substr(fileName.lastIndexOf('.') + 1);
        if (ext == "csv" && fileSize < 1000000) {
            this.fileStatus = false;
        }
        else {
            this.fileStatus = true;
            //this.cancelAll(event);
        }
    }

    onfileClick(event) {
        event.preventDefault();
        let initializevalue: any = this.inputFile;
        initializevalue.nativeElement.value = null;
        this.inputFile = initializevalue;

    }

    open(modal, event) {
        this.changeDetector.detectChanges();
        event.preventDefault();

        // this.modalService.open(modal);
        //this.bsModalRef = this.modalService.show(ModalContentComponent, {initialState});
        // this.modalService.open(modal);
        this.changeDetector.detectChanges();
        // this.modalReference = this.modalService.open(content,{size:'sm'});
        this.modalReference = this.modalService.open(modal);
        this.modalReference.result.then((result) => {
            //this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onfileChange(event) {
        event.preventDefault();
        this.uploadContainer = true;
        this.fileName = event.target.files[0];

    }
    onfileSubmit(content, event: any) {
        console.log(content);
        let filetype;
        if (this.fileName && this.fileName.type) {
            filetype = this.fileName.type;
        }
        else {
            if (this.fileName && this.fileName[0] && this.fileName[0].type) {
                filetype = this.fileName[0].type;
            }
            else {
                filetype = this.fileName.name.split('.').pop();
            }
        }

        let uplaodedfile = this.fileName[0] ? this.fileName[0] : this.fileName;
        let filename = this.fileName.name;
        if (filetype == "text/csv" || filetype == "csv" || filetype == "application/pdf"
            || filetype == "application/json" || filetype == "json" || filetype == "xlsx" || filetype == "application/vnd.ms-excel"
            || filetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            // this.uploadStatus = true;
            // this.fileStatus = false;
            console.log("in submit");
            this.loading = true;
            this.service.uploadcsvFile(uplaodedfile, filename, this.uuid, this.autorisation).then(
                res => {
                    console.log(res);
                    //if (res.status.kode != 9) {
                    if (res && res.response && res.response.success) {
                        this.serviceErrorMessage = res.response.success;
                        this.successNotification.refresh();
                        this.successNotification.open();
                        this.uploadStatus = false;
                        this.fileName = null;
                        this.uploadContainer = false;
                        this.loading = false;
                        this.modalReference.close();
                    }
                    else {
                        $('.errormessage').text(res.status.besked);
                        this.serviceErrorMessage = res && res.status && res.status.besked ? res.status.besked : "Error";
                        this.errorNotification.refresh();
                        this.errorNotification.open();
                        this.uploadStatus = false;
                        this.fileName = null;
                        this.uploadContainer = false;
                        this.loading = false;
                        this.modalReference.close();
                    }

                },
                error => {
                    this.errorNotification.refresh();
                    this.errorNotification.open();
                    this.uploadStatus = false;
                    this.fileName = null;
                    this.uploadContainer = false;
                    this.loading = false;
                }
            );
        }
        else {
            this.fileStatus = true;
            this.uploadContainer = false;
            console.log("in cancel ");
            let uploadFile: any = this.uploadfileForm;
            //uploadFile.nativeElement[0].files[0] = null
            this.fileName = null;
            this.uploadContainer = false;
            this.loading = false;
            setTimeout(() => {
                this.fileStatus = false;
            }, 4000);
        }

    }
    fileuploadCancel(event) {
        event.preventDefault();
        this.uploadContainer = false;
        console.log("in cancel ");
        let uploadFile: any = this.uploadfileForm;
        this.fileName = null;
        this.uploadContainer = false;

    }

    getValueFromObservable(): void {
        let contextid;
        contextid = this.services.getContext() ? this.services.getContext() : this.clickedrow;
        this.service.getGriddata(this.uuid, this.gridData, this.autorisation, contextid).then(
            res => { this.gridrowsandColumns = res; this.setdata() },
            error => { 
                this.errorNotification.refresh();this.errorNotification.open(); this.errorMessage = error.status.besked; console.log(error); this.loading = false; },
            complete => this.dataReceived()
        );

    }

    dataReceived(): void {
        console.log(this.gridrowsandColumns);
    }
    breadCrum() {
        if (!this.activatedRoute.snapshot.data.issubmenu) {
            console.log("submenu false");
            this.services.clearNavigation();
            this.services.setselectedMenu(this.heading);
            if (this.services.getContext() != null) {
                this.services.removeNavigation();
            }
            this.services.addNavigation(this.heading);

        }
        if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() == null) {
            console.log("submenu true");
            this.services.setselectedMenu(this.heading);
            if (this.services.getContext() == null && this.services.getNavigation().length > 1) {
                this.services.removeNavigation();
            }
            this.services.addNavigation(this.heading);
        }
        if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() != null) {
            this.navigationElements = this.services.getNavigation();
            this.navigationElements.length = 2;
            this.navigationElements.length = (this.navigationElements.length > 2) ? 2 : this.navigationElements.length;
            this.services.resettNavigation(this.navigationElements);
            this.services.addNavigation(this.heading);
        }
        this.navigationElements = this.services.getNavigation();


    }
    setdata(): void {
        this.breadCrum();
        let imageUrl = this.environmentUrl.substring(0, this.environmentUrl.indexOf('/api'));
        let grid: any = this.gridrowsandColumns;
        this.groupableValue = ['' + grid.result.column_group_name ? grid.result.column_group_name : ''];
        this.rows = new RowsList(grid.result.objekt_liste);
        var imgtag = this.rows;
        for (let img in imgtag) {
            if (imgtag[img].image) {
                imgtag[img].image = '<img style="width: 8em;height: 8em;" src="' + imageUrl + '' + imgtag[img].image + '"/>';
                if (imgtag[img].klassedokument) {
                    imgtag[img].klassedokument = imgtag[img].image;
                }
            }
        }
        this.rows = imgtag;
        this.datafileds = new DataFields(grid.result.datafields);
        this.columnss = new Columns(grid.result.columns);
        this.objektType = new ObjectType(grid.result.hovedobjekt);
        this.generateGrid(this.rows, this.columnss, this.datafileds)
        var imgtag = this.rows;
    }
    generateGrid(row, columndata, datafields) {
        this.loading = false;
        var datafield_item;
        var formatted_datafields = [];
        for (let d in datafields) {
            let item = datafields[d];
            item.replace(/"/g, '');
            eval('datafield_item=' + item);
            formatted_datafields.push(datafield_item);
        }
        var formatted_columnsfields = [];
        var columnfield_item;
        for (let c in columndata) {
            let item = columndata[c];
            item.replace(/"/g, '');
            eval('columnfield_item=' + item);
            formatted_columnsfields.push(columnfield_item);
        }
        this.source =
            {
                localdata: row,
                datafields: formatted_datafields,
                datatype: "array"
            };
        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.settings = {
            showfilterrow: true,
            sortable: true,
            pageable: true,
            pagermode: 'simple',
            autoloadstate: true,
            autosavestate: true,
            //column_group_name: 'opgaver',
            pagesize: 20,
            columnsresize: true,
            autoRowHeight: true,
            theme: 'ui-smoothness',
            filterable: true,
            enableellipsis: true,
            enablehover: true,
            enabletooltips: true,
            autoshowfiltericon: true,
            autoshowloadelement: true,
            selectionmode: 'singlerow',
            //columnresize: true,
            columnsreorder: true,
            groupable: true,
            groups: this.groupableValue


        };
        this.columns = formatted_columnsfields;
        // $("jqxgrid div").first().jqxGrid('localizestrings',this.localization.localizationobj);
        // console.log(this.localization)
        var state = this.services.getgridState();
        console.log(state);
        if (state) {
            setTimeout(() => {
                this.itsystemGrid.loadstate(state);
            }, 10);
        }
        this.itsystemGrid.createWidget(this.settings);

        // addfilter(dataField: string, filterGroup: any, refreshGrid?: false): void {
        //   this.itsystemGrid.loadstate(this.services.getgridState());
        //  this.itsystemGrid.applyfilters();



    }
    Filter(event: any): void {
        event.preventDefault();
        this.services.setgridState(this.itsystemGrid.savestate());
    }
    Sort(event: any): void {
        event.preventDefault();
        this.services.setgridState(this.itsystemGrid.savestate());
    }
    Rowclick(event: any): void {
        event.preventDefault();
        let submenu = this.services.gethasSubmenu();
        if (event.args.row.bounddata.uuid) {
            let id = event.args.row.bounddata.uuid;
            this.services.setClickedRowuuiid(id);
            if (submenu) {
                this.services.setContext(id);
            }
            console.log(event.args.row.bounddata.brugervendtnoegle);
            this.router.navigate(['' + this.selectedMenu + '/DetailsPage', { issubmenu: this.activatedRoute.snapshot.data.issubmenu }]);
        }
    }
    filtereddata(e) {
        event.preventDefault();
        console.log(this.itsystemGrid.getdisplayrows());
    }
    CreatePage(event: any): void {
        event.preventDefault();
        //this.router.navigate(['CreatePage', this.uuid]);
        this.router.navigate(['' + this.selectedMenu + '/CreatePage']);
        //this.router.config.push({ path: item.brugervendtnoegle + '/:CreatePage', component: CreatePage, data:{'listId':item.uuid} });
    }
    // exportdata(dataType: string, fileName?: string, exportHeader?: boolean, rows?: number[], exportHiddenColumns?: boolean, serverURL?: string, charSet?: string): any


    download(event: any): void {
        event.preventDefault();
        this.loading = true;
        let checkrows: any = this.itsystemGrid.getdisplayrows();//this.rows;
        let requestedfiletype = event.target.id;
        let allrows = [];
        for (let i in checkrows) {
            allrows.push(checkrows[i]);
        }
       
      
        if (event.target.id == "xlsx") {
            var excelcolumns = [];
            this.columns.map(function (item) {
                excelcolumns.push({ label: item.text, value: item.datafield });
            });
            var settings = {
                sheetName: this.selectedMenu,
                fileName: this.selectedMenu
            }
            if(checkrows.length===0){
                allrows.push(['']);
            }
            xlsx(excelcolumns, allrows, settings);
        }
        else {
            this.itsystemGrid.exportdata(event.target.id, '' + this.selectedMenu, true, allrows, false, 'https://uat-admin.carex.dk/pictures/save-file.php','utf-8');
        }
        this.successNotification.refresh();
        this.successNotification.open(); 
        this.loading = false;
        // below service can also be used to export data
        // this.service.downloadcsvFile(this.uuid,this.autorisation, requestedfiletype).then(
        //     res => { this.downloadFile(res); this.successNotification.open(); },
        //     error => { this.loading=false; this.errorNotification.open(); },
        // );
    }


    downloadFile(res) {
        this.documentuuid = res.result.filnavn;
        this.filepath = res.result.filpath;
        this.service.exportFileDocument(this.uuid, this.documentuuid, this.filepath, this.autorisation).then(
            res => {
                if (res.status.kode != 9) {
                    this.downloadDetails = res; this.getFile(res);  this.successNotification.refresh();this.successNotification.open(); this.loading = false;
                }
                else {
                    $('.errormessage').text(res.status.besked);
                    this.serviceErrorMessage = res.status.besked;
                    this.errorNotification.refresh();
                    this.errorNotification.open();
                    this.loading = false;
                }


            },
            error => {
                this.errorNotification.refresh();
                this.errorNotification.open(); this.errorMessage = error.status.besked; console.log(error); this.loading = false;
            }
        );
    }
    getFile(result) {
        let temp_Url = result.result.temp_path;
        let envi = this.service.getEnvironment();
        let e = envi.indexOf('/api/');
        let formurl = envi.substring(0, e);
        let tempindex = temp_Url.lastIndexOf('temp');
        let filedownload_url = temp_Url.substring(tempindex, temp_Url.length);
        let downloadURL = formurl.concat("/" + filedownload_url);
        console.log(downloadURL);
        // window.open(downloadURL);
        console.log(window);
        window.location.replace('' + downloadURL, '_blank');
    }

    upload(event: any) {
        event.preventDefault();
        this.uploadContainer = true;
    }
    onDrop(event) {
        this.changeDetector.detectChanges();
        event.preventDefault();
        let file = event.dataTransfer.files;
        var v = this.Modal.elementRef.nativeElement.ownerDocument.forms[0];
        v["0"].files = file;
        this.fileName = file;
        console.log(this.inputFile);
        console.log(file);
        console.log(event);
    }
    // ondragEnter(event) {
    //     event.preventDefault();
    //     console.log(event);
    // }
    // ondragLeave(event) {
    //     event.preventDefault();
    //     console.log(event);
    // }
    // ondragOver(event) {
    //     event.preventDefault();
    //     console.log(event);
    // }

}