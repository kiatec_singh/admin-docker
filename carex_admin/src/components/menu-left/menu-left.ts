import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Http } from '@angular/http';
import { Menu, MenuListItems } from '../../models/menu.items';
import { BaseRestService } from '../../providers/base.rest.service';
import { UserData } from '../../models/user.model';
import { Router } from '@angular/router';
import { Itsystems } from '../../components/itsystems/itsystems';
import { GanttChart } from '../../components/ganttchart/ganttchart';
import { DetailsPage } from '../../components/detailspage/detailspage';
import { CreatePage } from '../../components/createpage/createpage';
import { ConfigurationPage } from '../../components/configurationpage/configurationpage';
import { appDetailsPage } from '../app-detailspage/app-detailspage';
import { AuthRestService } from '../../providers/auth.rest.service';
import { statisticsComponent } from '../statistics/statistics';
import { Services } from '../../providers/services/services';

@Component({
    selector: 'menu-left',
    templateUrl: 'menu-left.html',
    providers: [BaseRestService, Http]
})

export class MenuLeft implements OnInit {

    private menuitems: Menu;
    private userdata: UserData;
    private menulist: MenuListItems[];
    private usermenulist: MenuListItems[];
    private menuItems;
    public submenulist: any[] = [];
    public client = "tryg";
    private mymenuItems;
    private clientMenu;
    private buttonsMennu = [];
    private autorisation = '';
    private userobj: any;

    constructor(private baserestService: BaseRestService, private ref:ChangeDetectorRef,
        private auth: AuthRestService, private router: Router, private location: Location, private services: Services) {
        this.auth.userobj.subscribe(
            (userobj) => {
                    this.userobj = userobj;
                    this.generateMenu();
                })
    }
    ngOnInit() {

    }

    generateMenu() {

        if (this.userobj && this.userobj.id) {
        this.baserestService.getAllMenuList().then(
            menulist => {
                //this.menulist = menulist.result;
                this.autorisation = menulist.autorisation;
                this.auth.setauthorisation(menulist.autorisation);
                this.configureRouting(menulist.result);
                if (menulist.relationer) {
                    this.auth.setuserrelations(menulist.relationer);
                }
                this.ref.detectChanges();
            },
            error => { console.log("something went wrong") }

        );
        }
        else{
            this.menulist=[];
        }
        //this.configureRoutingAppclientMenu();
        // this.Services.getMenuItems().then(
        //     mymenuItems => { this.mymenuItems = mymenuItems; this.configureRoutingAppMenu(); },
        //     error => { console.log(error) }
        // )
        // this.Services.getCustomerData().then(
        //     clientMenu => { this.clientMenu = clientMenu; this.configureRoutingAppclientMenu(); },
        //     error => { console.log(error) }
        // )

    }
    // configureRoutingAppMenu() {
    //     console.log(this.mymenuItems);
    // }
    configureRoutingAppclientMenu() {
        this.clientMenu = this.clientMenu[1];
        // this.buttonsMennu = this.clientMenu.buttons;
        for (let i in this.clientMenu.buttons) {
            let item = this.clientMenu.buttons[i];
            this.buttonsMennu.push(item[1]);
            this.router.config.push({ path: item[1], component: appDetailsPage, data: { 'item': item, 'title': item[1] } });
        }
    }


    configureRouting(resultmenu) {
        let makemenu: any = [];
        for (var key in resultmenu) {
            if (resultmenu.hasOwnProperty(key)) {
                var val = resultmenu[key];
                makemenu.push(val);
            }
        }
        this.menulist = makemenu;
        this.menulist.map(this.getRoutingItem, this);
        this.router.config.push({ path: 'ConfigureApp', component: ConfigurationPage });
        this.router.config.push({ path: 'statistics', component: statisticsComponent });
        //  this.usermenulist.push(this.menulist.map(this.getRoutingItem, this));
    }

    createuserMenu(item) {
        if (item.brugervendtnoegle == "Tilstande") {
            return item;
        }
    }
    getRoutingItem(item) {
        if (item.submenu) {
            console.log(this.submenulist);
            let submenuitems = [];
            let subitem = '';
            for (let anObject in item.submenu) {
                submenuitems.push(item.submenu[anObject]);
                subitem = item.submenu[anObject];
                if (item.submenu[anObject].titel == "Forløb") {
                    this.router.config.push({ path: item.brugervendtnoegle + '/' + item.submenu[anObject].brugervendtnoegle, component: GanttChart, data: { 'listId': item.submenu[anObject].uuid, 'title': item.submenu[anObject].titel, 'issubmenu': true } });
                }
                else {
                    this.router.config.push({ path: item.brugervendtnoegle + '/' + item.submenu[anObject].brugervendtnoegle, component: Itsystems, data: { 'listId': item.submenu[anObject].uuid, 'title': item.submenu[anObject].titel, 'issubmenu': true } });
                    this.router.config.push({ path: item.brugervendtnoegle + '/' + item.submenu[anObject].brugervendtnoegle + '/:uuid', component: DetailsPage, data: { 'issubmenu': true } });
                }

            }
            item.submenu = submenuitems;
        }
        if (item.titel == "Forløb") {
            this.router.config.push({ path: item.brugervendtnoegle, component: GanttChart, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': true } });
        } else {
            this.router.config.push({ path: item.brugervendtnoegle, component: Itsystems, data: { 'listId': item.skema_uuid, 'autorisation': this.autorisation, 'title': item.titel, 'issubmenu': false } });
            this.router.config.push({ path: item.brugervendtnoegle + '/CreatePage', component: CreatePage });
            //his.router.config.push({ path: item.brugervendtnoegle + '/:uuid', component: DetailsPage, data:{'listId':item.uuid, 'title': item.titel} });
            this.router.config.push({ path: item.brugervendtnoegle + '/:uuid', component: DetailsPage, data: { 'issubmenu': false } });
        }
    }

    getsubmenuroutingItem(item) {
        console.log(this.submenulist);
        var usersJson: any[] = Array.of(item.submenu);
        console.log(usersJson);
        return usersJson;
        // this.router.config.push({ path: item.brugervendtnoegle , component: Itsystems , data:{'listId':item.uuid, 'title': item.titel}});
        //this.router.config.push({ path: item.brugervendtnoegle + '/:uuid', component: DetailsPage });
    }
    active(event: any, item, uuid, name) {
        event.preventDefault();
        console.log(item);
        this.services.setContext(null);
        this.services.setgridState(null);
        if (item.submenu) {
            this.services.sethasSubmenu(true);
        }
        if (!item.submenu) {
            this.services.sethasSubmenu(false);
        }
        if (name != "personliste")
            this.router.navigate(['' + name]);
    }
    activeapp(event: any, item) {
        this.router.navigate(['' + item]);
    }
    activesubmenu(event: any, item, uuid, name) {
        event.stopPropagation();
        // if(item.submenu){
        //     this.services.sethasSubmenu(true);
        // }
        // if(!item.submenu){
        //     this.services.sethasSubmenu(false);
        //     this.services.setContext(null);
        // }
        console.log(name);

    }
    activeConfiguration(name) {
        this.router.navigate(['ConfigureApp']);
    }
}