import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'message-item',
  templateUrl: 'message-item.html'
})
export class MessageItem{

  @Input('message')
  private message: Message;

  constructor() { }

  ngOnInit() {
  }

}