import { Component, Output,Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
// import { OtpPage } from '../../pages/otp/otp';
// import { LoginPage } from '../../pages/login/login.page';
import { MenuLeft } from '../menu-left/menu-left';


// declare let google: any;

@Component({
    selector: 'statistics-viewer',
    templateUrl: 'statistics.html'
})
export class statisticsComponent {

    private alldomainsdata: any;
    private myData=[];
    private myColumnNames:any;
    private options:any;
    private pieoptions:any;
    private type="ColumnChart";
    private bartype="PieChart";
    private chartdata=[];
    private Dashboard="Dashboard";
    @Input() domainname: any;
    @Output() onChange = new EventEmitter()

    constructor(private fb: FormBuilder, private router: Router, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {

        this.baserestService.getstatistics().then(
            (alldomainsdata) => {this.alldomainsdata = alldomainsdata;       this.createChart();
                 }
        )
    }

    ngOnInit() {
        // Tracking
        //this.environment = this.auth.getEnvironment();
        // this.getloginData();

        // setTimeout(() => {
        //       google.charts.load('current', { 'packages': ['bar'] });
        // google.charts.setOnLoadCallback(this.createChart());
        // }, 1000);
  
      
    }
createChart() {

        // this.myData = [
        //     ['London', 8136000],
        //     ['New York', 8538000],
        //     ['Paris', 2244000],
        //     ['Berlin', 3470000],
        //     ['Kairo', 19500000]
        //   ]; 
         this.options = {
            is3D: true,
            width: 1200,
            height: 600,
            bar: {groupWidth: "100%"},
           legend: { position: "none" },
            colors: ['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99','#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],

          };
          this.pieoptions = {
            is3D: true,
            width: 1200,
            height: 600,
            colors: ['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99','#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],

          };
         
        //this.chartdata.push(['Element', 'Density', { role: 'style' }, { role: 'annotation' }])

         for(let d in this.alldomainsdata){
            if(this.alldomainsdata[d].domain){
                let bgcolor =  Math.floor(Math.random()*16777215).toString(16);
                bgcolor =  '#'+bgcolor;

                this.chartdata.push([this.alldomainsdata[d].domain, +this.alldomainsdata[d].total])
            }
         }
         console.log(this.chartdata);
         console.log(this.myData);
this.myData = this.chartdata;

        
}
getRandomColor(){
    let bgcolor =  Math.floor(Math.random()*16777215).toString(16);
    return '#'+bgcolor;
}
handleChange(name){
    console.log(name);
}
}
