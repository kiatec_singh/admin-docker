import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BaseRestService } from '../../providers/base.rest.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthRestService } from '../../providers/auth.rest.service';


@Component({
    selector: 'login',
    templateUrl: 'login.html',

})

export class Login {

    private userkey;
    private passwordkey;
    private error=false;

    constructor(private router:Router,private baserestService:BaseRestService,private authService:AuthRestService, private storageService:StorageService) {

    }

    ngOnInit(): void {
       
    }
    login(){
        this.baserestService.login(this.userkey,this.passwordkey).then(
            data=>{ 
                let udata:any=data;
                this.storageService.set('carexadmin',data);
                this.router.navigateByUrl('');
                this.authService.setuserobj(data);
            },
            error=>{this.error=true}
        )
    }
}