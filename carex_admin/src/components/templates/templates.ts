import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { ClipboardService } from 'ngx-clipboard';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

declare var $: any;

@Component({
    selector: 'templates',
    templateUrl: 'templates.html'
})

export class Templates {
    private userrelations: any;
    private skabelonlist: any;
    private skabeloncontent: any;
    private modalReference: any;
    private Objekttype: any;
    private slctorg: any = "vaælge";
    private skebelon: any = "vaælge";
    private title = '';
    private teaser = '';
    private beskrivelse = '';
    private flettefelter = "[]";
    private body: any;
    private slctlang = 'da';
    private loading = false;
    private authorisation;
    private skemaList;
    private sorteduserrelations;
    private slcOrgname: any;
    private slctbrugervendt: any;
    private orgcopyflag: boolean = false;
    private brugervendcopyflag: boolean = false;
    private textcopyflag: boolean = false;
    private jsoneditor;
    private changedjsondata;

    @ViewChild('successNotification', { static: false }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: false }) errorNotification: jqxNotificationComponent;
    public editorOptions: JsonEditorOptions;
    // public data: any;
    @ViewChild(JsonEditorComponent, { static: true }) editor: JsonEditorComponent;

    constructor(private baserestService: BaseRestService, private auth: AuthRestService, private clipboardService: ClipboardService) {
        this.editorOptions = new JsonEditorOptions();
        this.auth.autorisation.subscribe(
            (authorisation) => {
                {
                    this.authorisation = authorisation;
                }
            }
        );
        this.auth.userrelations.subscribe(
            (userrelations) => {
                {
                    this.userrelations = userrelations;
                }
            }
        );

    }

    ngOnInit() {
        console.log("in Translation");
        console.log(this.userrelations);
        let sortlist = this.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    }
    sortList(userrelations) {
        let sorted = userrelations.sort(function (a, b) {
            if (a.org && b.org) {
                var nameA = a.org.toLowerCase(), nameB = b.org.toLowerCase();
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;
            }
        });
        return sorted;
    }
    ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        $('select').selectpicker();
       
    }
    onChange(e, value) {
        this.loading = true;
        console.log(e.target.value);
        this.skemaList = null;
        this.skabeloncontent = null;
        let item = this.sorteduserrelations.filter(x => x.organisation_uuid == this.slctorg)[0];
        this.slcOrgname = item.org;
        this.baserestService.getskabelonlist(this.slctorg).then(
            (skabelonlist) => { this.skabelonlist = skabelonlist; this.renderSkema(); },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    renderSkema() {
        if (this.skabelonlist && this.skabelonlist.length > 0) {
            let sortedList = this.sortbyAlpha(this.skabelonlist);
            this.skemaList = sortedList;
            setTimeout(() => {
                $('select').selectpicker();
            });
        }
        this.loading = false;
    }
    sortbyAlpha(skabelonlist) {
        let sorted = skabelonlist.sort(function (a, b) {
            var nameA = a.brugervendtnoegle.toLowerCase(), nameB = b.brugervendtnoegle.toLowerCase();
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
        return sorted;

    }
    onChangelang(e) {
        this.loading = true;
        this.slctlang = e.target.value;
        this.baserestService.getskabeloncontent(this.skebelon, this.slctlang).then(
            (skabeloncontent: any) => { this.skabeloncontent = skabeloncontent; this.setData() },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    onchangeSkabelon() {
        this.loading = true;
        let item = this.skabelonlist.filter(x => x.uuid == this.skebelon)[0];
        this.slctbrugervendt = item.brugervendtnoegle;
        this.baserestService.getskabeloncontent(this.skebelon, this.slctlang).then(
            (skabeloncontent: any) => { this.skabeloncontent = skabeloncontent; this.setData() },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    setData() {
        this.loading = false;
        this.title = this.skabeloncontent.title;
        this.body = this.skabeloncontent.body;
        this.changedjsondata = this.skabeloncontent.body;
        this.teaser = this.skabeloncontent.teaser;
        this.beskrivelse = this.skabeloncontent.beskrivelse;
        this.jsoneditor = this.skabeloncontent.jsoneditor;
        this.flettefelter = this.skabeloncontent.flettefelter ? this.skabeloncontent.flettefelter : "[]";
        $('select').selectpicker();
        //this.slctlang = this.skabeloncontent.sprog ?this.skabeloncontent.sprog:'da' ;
    }
    Copyorg(flag) {
        this.orgcopyflag = false; this.brugervendcopyflag = false; this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
        //   this.clipboardService.copyFromContent(this.slcOrgname);
    }
    copied(e) {
        console.log(e);
    }
    Cancel(e) {
        e.preventDefault();
        this.skabelonlist.length = 0;
        this.skabeloncontent = null;
    }
    saveSkabelon() {
        this.loading = true;
        let bodydata:any;
        if(this.jsoneditor===true){
            bodydata= JSON.stringify(this.changedjsondata);
        }else{
            bodydata = this.body;  
        }
        this.baserestService.saveskabeloncontent(this.skebelon, this.title, this.teaser, this.beskrivelse, this.flettefelter, this.slctlang, bodydata).then(
            (savedcontent) => {
                console.log(savedcontent)
                this.successNotification.open(); this.loading = false;
            },
            (error) => {
                console.log(error);
                this.errorNotification.open(); this.loading = false;
            })
    }
    getData(e){
        this.changedjsondata=e;
    }
}