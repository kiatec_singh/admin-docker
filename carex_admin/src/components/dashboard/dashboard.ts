import { Component } from '@angular/core';

declare var jquery: any;
declare var $: any;
declare var google: any;
@Component({
    selector:'dashboard',
    templateUrl: 'dashboard.html'
})

export class Dashboard{

    constructor(){
        google.charts.load("current", {packages:["corechart"]});
        //google.charts.load('current', {packages:['gantt']});
      }
      ngAfterViewInit(){
        google.charts.setOnLoadCallback(this.drawChart);
        google.charts.setOnLoadCallback(this.drawChart2);
        //google.charts.setOnLoadCallback(this.drawChartGantt)
      }
      drawChart() {         
          
        
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
            ['Commute',  2],
            ['Watch TV', 2],
            ['Sleep',    7]
          ]);
  
          var options = {
            title: 'My Daily Activities',
            is3D: true,
          };
  
          var chart1 = new google.visualization.PieChart(document.getElementById('donutchart'));
          // selectHandler() function.
          
          chart1.draw(data, options);
        //   google.visualization.events.addListener(chart1, 'select', function () {
        //     var selectedBar = chart1.getSelection();
        //     ////console.log(selectedBar);
        //     //data.getRowLabel(selectedBar[0].row);
        //     console.log(selectedBar);
        //    /// for Bar charts, normally row-parameters are used
        //    /// manipulate on 'data' which refers to data-table to get desired results

        // });


          //google.visualization.events.addListener(chart, 'select',selectHandler);
        // var data = google.visualization.arrayToDataTable([['Task', 'Hours per Day'], ['Work', 11], ['Eat', 2], ['Commute',  2], ['Watch TV', 2], ['Sleep', 7] ]);          
        // var options = { title: 'My Daily Activities', pieHole: 0.2 };          
        // var chart = new google.visualization.PieChart(document.getElementById('donutchart'));         
        // chart.draw(data, options);
        // function selectHandler(){
        //   var selection = chart.getSelection();
        //   console.log(selection);
        //   var person = prompt("Please enter your name", "Harry Potter");
        // if (person != null) {
        
        //    console.log("Hello " + person + "! How are you today?");
        // }
        //  }
       
     }
     
  

     drawChart2() {  
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", { role: "style" } ],
        ["Monday", 8.94, "#b87333"],
        ["Tuesday", 10.49, "#06B73C"],
        ["Wednesday", 19.30, "#FD8600"],
        ["Thursday", 21.45, "color: #644591"],
        ["Friday", 8.94, "#61A8DF"],
        ["Saturday", 10.49, "#C22933"],
        ["Sunday", 19.30, "#FDCC46"]
      ]);
    
      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
    
      var options = {
        title: "UGE 32 2018",
        width: 900,
        height: 400,
        is3D: true,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        vAxis: { 
            ticks: [{v:0, f:'Activity1 Gå en tur..'},{v:10, f:'Activity2 Tage tapppen...'},{v:20, f:'Activity3 Ligge på rygge...'}]
   },
   pointSize: 30,
          series: {
                0: { pointShape: 'circle' },
                1: { pointShape: 'triangle' },
                2: { pointShape: 'square' },
                3: { pointShape: 'diamond' },
                4: { pointShape: 'star' },
                5: { pointShape: 'polygon' }
            }
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("barchart_values"));
      chart.draw(view, options);

     }
     drawChartGantt(){

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Task ID');
      data.addColumn('string', 'Task Name');
      data.addColumn('string', 'Resource');
      data.addColumn('date', 'Start Date');
      data.addColumn('date', 'End Date');
      data.addColumn('number', 'Duration');
      data.addColumn('number', 'Percent Complete');
      data.addColumn('string', 'Dependencies');

      data.addRows([
        ['2014Spring', 'Spring 2014', 'spring',
         new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
        ['2014Summer', 'Summer 2014', 'summer',
         new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
        ['2014Autumn', 'Autumn 2014', 'autumn',
         new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
        ['2014Winter', 'Winter 2014', 'winter',
         new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
        ['2015Spring', 'Spring 2015', 'spring',
         new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
        ['2015Summer', 'Summer 2015', 'summer',
         new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
        ['2015Autumn', 'Autumn 2015', 'autumn',
         new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
        ['2015Winter', 'Winter 2015', 'winter',
         new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
        ['Football', 'Football Season', 'sports',
         new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
        ['Baseball', 'Baseball Season', 'sports',
         new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
        ['Basketball', 'Basketball Season', 'sports',
         new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
        ['Hockey', 'Hockey Season', 'sports',
         new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
      ]);

      var options = {
        height: 400,
        gantt: {
          trackHeight: 30
        }
      };

      var chart = new google.visualization.Gantt(document.getElementById("gantt_chart"));

      chart.draw(data, options);
     }

     changeActivity(event){
   console.log(event);
   var emojistatus;
   var eventstatus = event.target.form;

   for(var v=0;v <= 3; v++){ 
     
   if(eventstatus[v].checked){ 
    if(eventstatus[v].value=="Not Done"){
      emojistatus= "red";
      $("#emoji1").removeClass();
      $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--red");
      $("#emoji1 i").removeClass();
      $("#emoji1 i").addClass("fa fa-frown-o");
    }
    if(eventstatus[v].value=="Partially"){
      emojistatus="yellow";
      $("#emoji1").removeClass();
      $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--yellow");
      $("#emoji1 i").removeClass();
      $("#emoji1 i").addClass("fa fa-meh-o");
      
    }
    if(eventstatus[v].value=="Done"){
      $("#emoji1").removeClass();
      $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--green");
      $("#emoji1 i").removeClass();
      $("#emoji1 i").addClass("fa fa-smile-o");
    }

   }
  
  }


      //   var person = prompt("Wannna change the state of Activity", "Done");

     }
}
