import {Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {data} from '../../models/data/data';
import {ActivatedRoute} from '@angular/router';


declare var jquery: any;
declare var $: any;

@Component({
    selector:'app-details',
    templateUrl: 'app-detailspage.html'
})
export class appDetailsPage {
   
    private customerName;
    private selectedItem;
    private heading;
    private item;
    private tabs;
    private tabsdata;
    private tabsContent: any = [];
    private selectedContainer = 0;
    private tabstoDisplay: any = [];
    private showsearchbutton = false;
    private searchContent;
    private loading: boolean;
    private view;
    private searchlabel;
    private externallink;
    private starttestLabel;
    private user;
    public selectedTab = 0;
    private allsearchresultList;
    private searchdropdownValue;
    private dropdownName;
    private resetlabel;
    private searchdropdownValueArray;
    private helbredskategorierModel;
    private emnerModel;
    private searchtext;
    private textfilteredList: any = [];
    private baseUrl;
    

    constructor(private activateroute: ActivatedRoute ) {
        //this.createForm();
      
    }

    ngOnInit() {
       this.selectedItem =  this.activateroute.snapshot.data.item;
        console.log("in app");
        this.heading = this.activateroute.snapshot.data.title;
        this.tabs = this.selectedItem[8];


        console.log(this.selectedItem);
        this.setData();
       
}
setData(){
    var tabsdata = [];
    let currentitem: any;
    for (let item in this.tabs) {
        currentitem = this.tabs[item];
        this.tabstoDisplay.push(currentitem.tab_name);
        if (currentitem.text) {
          tabsdata.push(currentitem.text);
        }
  
  
      }
      this.tabsContent = tabsdata[0];
      this.tabsdata = tabsdata;
}

// switchonTab(id) {
// this.selectedContainer = id;
// this.tabsContent = this.tabsdata[id];
// console.log(this.selectedContainer);
// let defaulttab: any = this.navbuttons;
// }

switchTab(event, id): void {

    event.preventDefault();
    this.selectedTab = id;
    //this.loading= true;
    this.selectedContainer = id;


}


}