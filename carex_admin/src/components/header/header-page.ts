import { Component, Inject } from '@angular/core';
import { AuthRestService } from '../../providers/auth.rest.service';
import { UserData } from '../../models/user.model';
import { DOCUMENT } from '@angular/common';
import { StorageService } from '../../providers/storageservice/storageservice';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
    selector: 'header-page',
    templateUrl: 'header-page.html'
})



export class HeaderPage {
    private user: UserData;
    constructor(private authService: AuthRestService, @Inject(DOCUMENT) private document: any,private router:Router, private storageService:StorageService) {
        this.authService.userobj.subscribe(
            (userobj) => {
                if (userobj && userobj.id) {
                    this.user = new UserData(userobj);
                    console.log(userobj);
                }else{
                    this.user = null;
                }
            });
    }

    ngOnInit() {
    }

    // setUserData(userinfo): void {
    //     let userinformation: any = userinfo;
    //     var obj = JSON.parse(userinformation);
    //     this.user = new UserData(obj);
    // }
    logout() {
        // this.router.navigateByUrl('/logout.php');
        this.storageService.clear();
        this.authService.setauthorisation(null);
        this.authService.setuserobj(null);
     //   this.document.location.href = "/logout.php";
         this.router.navigateByUrl('login');

    }
}