import { Component } from '@angular/core';
import { Message } from '../../models/message';
import{ DialogflowService} from '../../providers/services/dataflow';
import { concat } from 'rxjs/operators/concat';
import{Messages} from '../../models/messages';

@Component({
    selector: 'chat-container',
    templateUrl: 'chat-container.html'
})

export class ChatContainer {

    private message: Message[];
    private messages:any =[];
    private  chatenable:boolean = false;
    private chatquestion='';
    constructor(private dialogflowservice:  DialogflowService ) {

    }
    onInit() {
        this.messages.push(new Messages(''));
        console.log(this.message);
       
    }

    hide(event) {
        event.preventDefault();

        console.log("in hided");
    }

    close(event) {
        event.preventDefault();
        //this.state = this.state === 'active' ? 'inactive' : 'active';
         this.chatenable = this.chatenable ==true?  false: true;
        console.log("in close");
    }
    open(event) {
        event.preventDefault();
        //this.state = this.state === 'active' ? 'inactive' : 'active';
         this.chatenable = this.chatenable ==true?  false: true;
        console.log("in close");
    }
    sendchatMessage(event){
     console.log(this.chatquestion);
     this.messages.push(this.chatquestion);
     let cq = this.chatquestion;
     this.chatquestion='';
     this.dialogflowservice.sendChatMessage(cq).subscribe(Message=> this.message = Message);
     setTimeout(() => {
        console.log("Async Task Calling Callback");
        this.setMessages();
        // this.generate();
    }, 2500);
    }
    setMessages(){
        let chat:any= this.message;
        this.messages.push(chat.result.fulfillment.speech);
        console.log(this.message);
    }
}