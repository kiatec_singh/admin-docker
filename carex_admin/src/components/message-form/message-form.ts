import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message';
import { DialogflowService } from '../../providers/services/dataflow';

@Component({
    selector: 'message-form',
    templateUrl: 'message-form.html'
})
export class MessageForm {
    @Input('message')
    private message: Message;

    @Input('messages')
    private messages: Message[];
    private dialogFlowService: DialogflowService;

        constructor() { }

    /*   @Input('messages')
      private messages : Message[]; */

    ngOnInit() {
        console.log(this.message);
    }

    sendMessage(): void {
        this.message.timestamp = new Date();
        this.messages.push(this.message);
    
    /*     this.dialogFlowService.getResponse(this.message.content).subscribe(res => {
          this.messages.push(
            new Message(res.result.fulfillment.speech, 'assets/icons/user.png', res.timestamp)
          );
        });
    
        this.message = new Message('', 'assets/icons/user.png');
    } */

}}  