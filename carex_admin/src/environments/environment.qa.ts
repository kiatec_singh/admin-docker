export const environment = {
  production: false,
  baseURL :"https://qa-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://qa-idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://qa-idp.carex.dk/html/skabelonapi.php",
  version: '1.0.0'
};
