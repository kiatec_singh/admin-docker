export const environment = {
  production: false,
  baseURL :"https://demo-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://demo-idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://demo-idp.carex.dk/html/skabelonapi.php",
  version: '1.0.0'
};
