export const environment = {
  production: true,
  baseURL :"https://api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://idp.carex.dk/html/skabelonapi.php",
  version: '1.0.0'
};
