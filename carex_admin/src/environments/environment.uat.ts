export const environment = {
  production: true,
  baseURL :"https://uat-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://uat-idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://uat-idp.carex.dk/html/skabelonapi.php",
  version: '1.0.0'
};
