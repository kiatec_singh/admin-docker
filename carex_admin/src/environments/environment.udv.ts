export const environment = {
  production: false,
  baseURL :"https://udv-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://udv-idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://udv-idp.carex.dk/html/skabelonapi.php",
  version: '1.0.0'
};
