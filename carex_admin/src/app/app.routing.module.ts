import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Itsystems} from '../components/itsystems/itsystems';
import { DetailsPage } from '../components/detailspage/detailspage';
import { sendpushnotificationComponent } from '../components/sendpushnotification/sendpushnotification';
import { Translation } from '../components/translation/translation';
import { Templates } from '../components/templates/templates';
import { analyticsComponent } from '../components/analytics/analytics';
import { serviceanalyticsComponent } from '../components/serviceanalytics/serviceanalytics';
import { Login } from '../components/login/login';

const routes: Routes = [
    // {path:'Organisationer', component:Organization},
//     {path:'Organisationer/:listId', component:Itsystems},
{path:'Katalog TRYG', component:Itsystems},
{path:'tilstandliste/DetailsPage', component: DetailsPage },
{ path: 'sendpushNotification', component: sendpushnotificationComponent},
{ path: 'translation', component: Translation},
{ path: 'templates', component: Templates},
{ path: 'analytics', component: analyticsComponent},
{ path: 'serviceanalytics', component: serviceanalyticsComponent},
// { path: '', component: Login },
{ path: 'login', component: Login},
]; 

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{
ngOnInit() {
    console.log("in routereee....");

}
};