import { Component, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { RouterExtService } from '../providers/services/router.service';

 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Carex';

  private hostname;
  constructor(private location: Location, private storageService: RouterExtService) {
   var isloggedin = this.storageService.isloggedin();
console.log(isloggedin);

    }


  ngOnInit(): void {
    let loc: any = this.location
    this.hostname = loc._platformStrategy._platformLocation.location.hostname;
    console.log("in app initialed")
  }
}