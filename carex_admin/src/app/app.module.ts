//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgDatepickerModule } from 'ng2-datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationStrategy, HashLocationStrategy, APP_BASE_HREF, DatePipe } from '@angular/common';
import { DawaAutocompleteModule } from 'ngx-dawa-autocomplete';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { GoogleChartsModule } from 'angular-google-charts';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgJsonEditorModule } from 'ang-jsoneditor' 

// import { OWL_DATE_TIME_FORMATS} from 'ng-pick-datetime';
// import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { ClipboardModule } from 'ngx-clipboard';
import { MomentDateTimeAdapter, OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';


import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

export const MY_CUSTOM_FORMATS = {
fullPickerInput: 'DD-MM-YYYY',//'YYYY-MM-DD HH:mm:ss',
parseInput: 'DD-MM-YYYY',//'YYYY-MM-DD HH:mm:ss',
datePickerInput: 'DD-MM-YYYY',//'YYYY-MM-DD HH:mm:ss',
timePickerInput: 'DD-MM-YYYY',
monthYearLabel: 'DD-MM-YYYY',
dateA11yLabel: 'DD-MM-YYYY',
monthYearA11yLabel: 'DD-MM-YYYY',
}



//pages
import { HeaderPage } from '../components/header/header-page';
import { FooterPage } from '../components/footer/footer-page';
import { MenuRight } from '../components/menu-right/menu-right';
import { MenuLeft } from '../components/menu-left/menu-left';
import { Itsystems } from '../components/itsystems/itsystems';
import { DetailsPage } from '../components/detailspage/detailspage';
import { CreatePage } from '../components/createpage/createpage';
import { ConfigurationPage } from '../components/configurationpage/configurationpage';
import { Login } from '../components/login/login';
import { Widgets } from '../components/widgets/widgets';
import { MessageList } from '../components/message-list/message-list';
import { MessageItem } from '../components/message-item/message-item';
import { MessageForm } from '../components/message-form/message-form';
import { ChatContainer } from '../components/chat-container/chat-container';
import { Dashboard } from '../components/dashboard/dashboard';
import { Notifications } from '../components/notifications/notifications';
import { modalComponent } from '../components/modal/modal';
import { GanttChart } from '../components/ganttchart/ganttchart';
import { appDetailsPage } from '../components/app-detailspage/app-detailspage';
import {AppRoutingModule} from './app.routing.module';
import {BreadcrumComponent} from '../components/breadcrum/breadcrum';
import {sendpushnotificationComponent} from '../components/sendpushnotification/sendpushnotification';
import {statisticsComponent} from '../components/statistics/statistics';
import {Translation} from '../components/translation/translation';
import {Templates} from '../components/templates/templates';
import{domainstatisticsComponent} from '../components/domainstatistics/domainstatistics';
import{analyticsComponent} from '../components/analytics/analytics';
import{serviceanalyticsComponent} from '../components/serviceanalytics/serviceanalytics';



//components
import { AppComponent } from './app.component';

//models
import { MenuItems } from '../models/menu.items';
import { Localization } from '../models/localization/localization';
import { Message } from '../models/message';
import { Messages } from '../models/messages';
import { UserData } from '../models/user.model';
import { data } from '../models/data/data';



//providers
import { Services } from '../providers/services/services';
import { BaseRestService } from '../providers/base.rest.service';
import { AuthRestService } from '../providers/auth.rest.service';
import { DialogflowService } from '../providers/services/dataflow';
import { RouterExtService } from '../providers/services/router.service';
import { WindowRefService } from '../providers/window.ref';
import {StorageService} from '../providers/storageservice/storageservice';

//directives
import { RouterModule } from '@angular/router';




const modelDeclatarions = [

]

const pagesDeclarations = [

]

const componentsDeclarations = [
  HeaderPage,
  FooterPage,
  MenuLeft,
  MenuRight,
  GanttChart,
  Itsystems,
  DetailsPage,
  CreatePage,
  ConfigurationPage,
  Login,
  Widgets,
  MessageList,
  MessageItem,
  MessageForm,
  ChatContainer,
  Dashboard,
  appDetailsPage,
  Notifications,
  modalComponent,
  BreadcrumComponent,
  sendpushnotificationComponent,
  statisticsComponent,
  Translation,
  Templates,
  domainstatisticsComponent,
  analyticsComponent,
  serviceanalyticsComponent
]

const providersDeclarations = [
  Services,
  BaseRestService,
  AuthRestService,
  DialogflowService,
  RouterExtService,
  WindowRefService,
  StorageService,
  DatePipe

]
const pipeDeclarations = []

const directivesDeclarations = [
  
]


@NgModule({
  declarations: [
    AppComponent, jqxGridComponent,
    jqxTabsComponent,
    jqxFileUploadComponent,
    jqxDropDownListComponent,
    jqxCalendarComponent,
    jqxDateTimeInputComponent,
    jqxNotificationComponent,
    // OwlDateTimeModule, 
    // OwlMomentDateTimeModule,
    //globalization,
    //globalize,
    ...pagesDeclarations,
    // Models
    ...modelDeclatarions,
    // Components
    ...componentsDeclarations,
    // Directives
    ...directivesDeclarations,
    // Pipes
    ...pipeDeclarations

  ],
  entryComponents: [
    Itsystems,
    DetailsPage,
    CreatePage,
    Login,
    ConfigurationPage,
    modalComponent,
    GanttChart,
    appDetailsPage,
    BreadcrumComponent,
    statisticsComponent,
    Translation,
    Templates,
    analyticsComponent,
    serviceanalyticsComponent,
    domainstatisticsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    NgDatepickerModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    AmChartsModule,
    NgbModule,
    ClipboardModule,
    NgJsonEditorModule,
    RouterModule.forRoot([]),
    GoogleChartsModule.forRoot(),
    DawaAutocompleteModule.forRoot()
  ],
  providers: [
    ...providersDeclarations,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
{ provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
