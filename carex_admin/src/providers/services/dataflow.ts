import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Form } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import{Message} from '../../models/message';

@Injectable()
export class DialogflowService {

  private baseURL: string = "https://api.dialogflow.com/v1/query?v=20170712";
  private token: string = 'ef33f3994bdb463dadc7641ca5f07e63';
  private headers: Headers;
  private options: RequestOptions;
  private message: Message;

  constructor(private http: HttpClient) {

    this.options = new RequestOptions({ headers: this.headers });

}

private formdata;

sendChatMessage(chatquestion: string): Observable<Message[]> {
  this.headers = new Headers({
    "Authorization": "Bearer ef33f3994bdb463dadc7641ca5f07e63",
    "Content-Type": "application/json",
    "sessionId": "1d0c2c63-6872-4084-848c-0dfde1c9b087",
    "lang":"en"
});
  var settings = {
    "lang": "en",
    "query": chatquestion,
    "sessionId": "1d0c2c63-6872-4084-848c-0dfde1c9b087",
    "timezone": "Europe/Paris",
    "headers": {
      "Authorization": "Bearer ef33f3994bdb463dadc7641ca5f07e63",
      "Content-Type": "application/json"
    }
  }
    this.formdata = new FormData();
    this.headers.append("query", chatquestion)
    this.formdata.append('action', 'CALL_list_objekttyper');
    return this.http.post<Message[]>(this.baseURL, this.headers,settings).pipe(
        tap((data) => this.chatReply(data),
            () => console.log('error'))
    );
}

chatReply(message): void {
  console.log(message);
    this.message = new Message(message);
}
}