import { Injectable } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from "@angular/router";
import { StorageService } from '../storageservice/storageservice';

/** A router wrapper, adding extra functions. */
@Injectable()
export class RouterExtService {

  private previousUrl: string = undefined;
  private currentUrl: string = undefined;

  constructor(public router: Router, private storageService: StorageService) {
    this.currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      };
    });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }

  isloggedin() {
    let user = this.storageService.get('carexadmin');
    if (!user) {
      this.router.navigateByUrl('login');
    }
    else {
      return true;
    }
  }
}