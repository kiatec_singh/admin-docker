import { Injectable } from '@angular/core';

@Injectable()
export class Services {

    public listId;
    public rowId;
    public selectedMenu;
    public displayName;
    public heading;
    public navigationElements = [];
    public hasSubmenu;
    public context;
    public gridstate;
    constructor() {
    }


    public setlistId(uuid) {
        this.listId = uuid;
    }
    public getlistId() {
        return this.listId;
    }
    public setClickedRowuuiid(rowid) {
        this.rowId = rowid;
    }
    public getClickedRowuuiid() {
        return this.rowId;
    }


    public setselectedMenu(selectedMenuTitle) {
        return this.selectedMenu = selectedMenuTitle;
    }
    public getselectedMenu() {
        return this.selectedMenu;
    }
    public setdisplayName(displayName) {
        return this.displayName = displayName;
    }
    public getdisplayName() {
        return this.displayName;
    }
    public setheading(heading) {
        return this.heading = heading;
    }
    public getheading() {
        return this.heading;
    }


    public addNavigation(item) {
        this.navigationElements.push(item);
    }

    public removeNavigation() {
        this.navigationElements.pop();
    }
    public resettNavigation(item) {
        this.navigationElements = item;
    }
    public getNavigation() {
        return this.navigationElements;
    }
    public clearNavigation() {
        this.navigationElements.length = 0;
    }
    public sethasSubmenu(boolean) {
        this.hasSubmenu = boolean;
    }
    public gethasSubmenu() {
        return this.hasSubmenu;
    }
    public setContext(id) {
        this.context = id;
    }
    public getContext() {
        return this.context;
    }

    public getgridState() {
        return this.gridstate;
    }
    public setgridState(gridstate) {
        this.gridstate = gridstate;
    }
}


