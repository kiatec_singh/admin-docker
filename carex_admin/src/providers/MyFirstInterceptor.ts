import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap } from "rxjs/operators"


interface HttpProgressEvent { 
    type: HttpEventType.DownloadProgress | HttpEventType.UploadProgress
    loaded: number
    total?: number
  }

@Injectable()
export class MyFirstInterceptor { // implements HttpInterceptor {
    constructor() { }

    // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     console.log(JSON.stringify(req));

    //     // if (!req.headers.has('Content-Type')) {
    //     //     req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    //     // }
    //     req = req.clone({headers: new HttpHeaders({
    //         'Content-Type': 'application/x-www-form-urlencoded',
    //         'Access-Control-Allow-Origin': '*',
    //         'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
    //         'Accept': 'application/json',
    //         "Access-Control-Allow-Headers": "X-Requested-With",
    //         'Content-Length': '0'
    //     }), reportProgress: true});
    //    // const reportingRequest = req.clone({ reportProgress: true });

    // console.log(req);
    //     return next.handle(req).pipe(
    //         tap(event => {
    //             switch (event.type) {
    //                 case HttpEventType.Sent:
    //                 console.log(event);
    //                 console.log('Request sent!');
    //                 break;
    //               case HttpEventType.ResponseHeader:
    //               console.log(event);
    //                 console.log('Response header received!');
    //                 break;
    //               case HttpEventType.DownloadProgress:
    //               console.log(event);
    //                 const kbLoaded = Math.round(event.loaded / 1024);
    //                 console.log(`Download in progress! ${ event.loaded / 1024 }Kb loaded`);
    //                 break;
    //               case HttpEventType.Response:
    //               console.log(event);
    //                 console.log('😺 Done!', event.body);
    //                 return event.body;
    //             }
    //         }, error => {
    //             console.error('NICE ERROR')
    //         })
    //     )

    //     return next.handle(req).do((event: HttpEvent<any>) => {
    //         if (event instanceof HttpResponse) {
    //             // do stuff with response if you want
    //         }
    //     }, (err: any) => {
    //         if (err instanceof HttpErrorResponse) {
    //             if (err.status === 401) {
    //                 // redirect to the login route
    //                 // or show a modal
    //             }
    //         }
    //     });

    // }

}




