import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Form } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UserData } from '../models/user.model';



import { error } from 'util';
import { Http } from '@angular/http/src/http';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthRestService {
    headers: Headers;
    options: RequestOptions;
    form: Form;
    public userdata: UserData;
    public userrelations= new BehaviorSubject<any>([]);
    public client;
    public autorisation = new BehaviorSubject<any>([]);
    public userobj = new BehaviorSubject<any>([]);

    private baseUrl = 'https://qa-api.carex.dk/api/endpoints/api_services.php'; //'http://87.54.27.85/carex/qa-carexapi/api/endpoints/api_services.php' ;//'http://87.54.27.85/carex/udv-carexapi/api/endpoints/api_services.php'; //'http://87.54.27.85/carex/carex_api/qa/api/endpoints/api_services.php'; 
    // URL to web api
    constructor(private http: HttpClient) {
        this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
        });
        this.options = new RequestOptions({ headers: this.headers });

    }
    private formdata;


    getUserdata(uuid): Observable<UserData[]> {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_user_info');
        this.formdata.append('bruger_uuid', '504aa045-c85f-11e7-8f36-005056b35b6d');
        return this.http.post<UserData[]>(this.baseUrl, this.formdata).pipe(
            tap((data) => this.setUserData(data),
                () => console.log('error'))
        );
    }
    setUserData(data): void {
        console.log(data);
        this.userdata = new UserData(data.result[0]);
        console.log(this);
    }
    setuserrelations(userrelations) {
        this.userrelations.next(userrelations);
    }

    getClient() {
        return this.client;
    }
    setClient(client) {
        this.client = client;

    }
    setauthorisation(value: any) {
        this.autorisation.next(value);
    }
    setuserobj(value: any) {
        this.userobj.next(value);
    }
}