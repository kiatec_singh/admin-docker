import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Form } from '@angular/forms';
import { Observable } from 'rxjs';
import { Subscriber } from 'rxjs';
import 'rxjs/add/operator/map';
import { Menu, MenuListItems } from '../models/menu.items';
import { GridData, Columns, RowsList, DataFields, ObjectType } from '../models/grid.data';
import { UserData } from '../models/user.model';
import { DetailsData, Structure } from '../models/details.data';
import { environment } from '../environments/environment';
import { AuthRestService } from './auth.rest.service';


declare var jquery: any;
declare var $: any;

@Injectable()
export class BaseRestService {
    headers: Headers;
    private options;
    form: Form;
    private menu: Menu;
    private menuListItems: MenuListItems;
    private columns: Columns;
    private rows: RowsList;
    private objektType: ObjectType;
    private griddata: GridData;
    private datafileds: DataFields;
    private detailsdata: DetailsData;
    private structure: Structure;
    private userData: UserData;
    private authorisation;
    private subscriber: Subscriber<any>
    private loginservicesUrl = 'https://uat-idp.carex.dk/endpoints/login_services.php';
    private skabelonUrl;

    private baseUrl;// = 'https://udv-api.carex.dk/api/endpoints/api_services.php'; // 'https://87.54.27.85/carex/udv-carexapi/api/endpoints/api_services.php'; //'http://87.54.27.85/carex/carex_api/qa/api/endpoints/api_services.php'; 

    private prodUrl = "http://trygsundhed.carex.dk/";

    // URL to web api
    constructor(private http: HttpClient,private authService: AuthRestService) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            'Accept': 'application/json',
            "Access-Control-Allow-Headers": "X-Requested-With",
            'Authorization': 'key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr'

        });
        this.authService.userobj.subscribe(
            (userobj)=>{
                if(userobj){
                this.userData = new UserData(userobj);
                }
            });
        this.options = new RequestOptions({ headers: this.headers });
        console.log(environment.baseURL);
        this.baseUrl = environment.baseURL;
        this.skabelonUrl = environment.skabelonUrl;
        this.loginservicesUrl = environment.loginservicesUrl;
    }
    private formdata;

    getEnvironment() {
        return this.baseUrl;
    }

    getAllMenuList(): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_menuitems');
        // this.formdata.append('autorisation','e711d181-1986-4553-9832-94276f9034f3');
        //  this.formdata.append('autorisation', this.userData.uuid); //this.userData.uuid 'e221328e-5a47-4dd6-9289-36e13f8ede7c'
        this.formdata.append('bruger_uuid', this.userData.uuid); //'001dd209-6083-4feb-9b22-07607bcf3b2d'
        this.formdata.append('applikation_uuid', '48b00aae-5001-4b4f-9eea-b7bd61145686');
        return this.http.post<Menu[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }

    setMenuItems(menuitems): void {
        this.menu = new Menu(menuitems);
        this.menuListItems = new MenuListItems(menuitems);
    }

    getGriddata(uuid, griddata: GridData, autorisation, clickedrow): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('autorisation', autorisation); //this.userData.uuid   'e221328e-5a47-4dd6-9289-36e13f8ede7c'
        this.formdata.append('skema_uuid', uuid);
        // this.formdata.append('pageSize', 500);
        // this.formdata.append('page', 1);
        this.formdata.append('context', '1c812baf-8189-4f32-ad50-658b29006f0d');
        return this.http.post<GridData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    // getsubmenuGriddata(uuid, griddata: GridData, personid): any {
    //     this.formdata = new FormData();
    //     this.formdata.append('action', 'CALL_list_objekt_data');
    //     this.formdata.append('autorisation', this.userData.uuid);
    //     //documneet list id
    //     this.formdata.append('listedefinition_uuid', uuid);

    //     //selected person 
    //     this.formdata.append('context', personid);

    //     return this.http.post<GridData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    // }


    getDetailsdata(uuid, listId, autorisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('autorisation', autorisation); //e221328e-5a47-4dd6-9289-36e13f8ede7c'
        this.formdata.append('objekt_uuid', uuid);
        this.formdata.append('skema_uuid', listId);

        //this.formdata.append('objekttype', 'funktion');
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    getcreateDetailsdata(listId, autorisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('autorisation', autorisation); //e221328e-5a47-4dd6-9289-36e13f8ede7c'
        this.formdata.append('skema_uuid', listId);

        //this.formdata.append('objekttype', 'funktion');
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    downloadcsvFile(listId, autorisation, requestedfiletype): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_eksporter_objektdata');
        this.formdata.append('autorisation', autorisation);
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('requestedfiletype', requestedfiletype)
        //this.formdata.append('objekt_uuid', uuid);
        //this.formdata.append('objekttype', 'funktion');
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    uploadcsvFile(file, filename, listId, authid): any {
        this.formdata = new FormData();
        let o: any = new Observable(obs => this.subscriber = obs);
        this.formdata.append('action', 'CALL_importer_objektdata');
        this.formdata.append('autorisation', authid);
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        //this.formdata.append('objekt_uuid', uuid);
        //this.formdata.append('objekttype', 'funktion');
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    public cancelRequest() {
        this.subscriber.error();
    }


    updateDetailsdata(detailsuuid, listedefinition_uuid, authid, update_timestamp, updateddata, allfiles): any {
        this.formdata = new FormData();
        let objectuuid = listedefinition_uuid ? listedefinition_uuid : '';
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('autorisation', authid); //this.userData.uuid'e221328e-5a47-4dd6-9289-36e13f8ede7c'
        this.formdata.append('objekt_uuid', objectuuid);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (let i = 0; i < allfiles.length; i++) {
                let name = allfiles[i].name;
                let value = allfiles[i].value;
                this.formdata.append(name, value);
            }
            this
        }

        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    createDetailsdata(detailsuuid, listedefinition_uuid, authid, update_timestamp, updateddata, allfiles): any {
        this.formdata = new FormData();
        let objectuuid = listedefinition_uuid ? listedefinition_uuid : '';
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('autorisation', authid); //this.userData.uuid'e221328e-5a47-4dd6-9289-36e13f8ede7c'
        // this.formdata.append('objekt_uuid', objectuuid);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (let i = 0; i < allfiles.length; i++) {
                let name = allfiles[i].name;
                let value = allfiles[i].value;
                this.formdata.append(name, value);
            }
            this
        }

        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    downloadFileDocument(detailsuuid, listedefinition_uuid): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_file');
        //this.formdata.append('listedefinition_uuid', detailsuuid);
        this.formdata.append('autorisation', this.userData.uuid);
        this.formdata.append('dokument_uuid', listedefinition_uuid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    }
    exportFileDocument(detailsuuid, filenavn, path, auth): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_exportfile');
        //this.formdata.append('listedefinition_uuid', detailsuuid);
        this.formdata.append('autorisation', auth);
        this.formdata.append('filnavn', filenavn);
        this.formdata.append('filpath', path);
        this.formdata.append('itsystem_uuid', '48b00aae-5001-4b4f-9eea-b7bd61145686');
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    }
    deleteDetailsdata(detailsuuid, listedefinition_uuid, authid, update_timestamp): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_delete_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('autorisation', authid);
        this.formdata.append('objekt_uuid', listedefinition_uuid);
        this.formdata.append('update_timestamp', update_timestamp);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }

    setupdatedDetailsdata(data) {
        this.structure = new Structure(data.result.struktur);
        this.detailsdata = new DetailsData(data.result.bi_objekter);
        console.log(this);
    }
    SetDetailsData(data) {
        //this.tabs = new Tabs(data.result);
        // this.structure = new Structure(data.result.struktur);
        this.detailsdata = new DetailsData(data.result);
        console.log(this);
    }
    checklogin() {
        return this.http.get("https://test-tryg.carex.dk/", this.options).toPromise();
    }

    getstatistics() {
        this.formdata = new FormData();
        this.formdata.append('action', 'statistics');
        this.formdata.append('id', 'e221328e-5a47-4dd6-9289-36e13f8ede7c'); //e221328e-5a47-4dd6-9289-36e13f8ede7c'
        //this.formdata.append('objekttype', 'funktion');
        return this.http.post(this.loginservicesUrl, this.formdata).map(res => res).toPromise();

    }
    getdomainstatistics(name) {
        this.formdata = new FormData();
        this.formdata.append('action', 'domainstatistics');
        this.formdata.append('name', name);
        return this.http.post(this.loginservicesUrl, this.formdata).map(res => res).toPromise();

    }
    getuseranalytics(){
        this.formdata = new FormData();
        this.formdata.append('action', 'getuseranalytics');
        return this.http.post(this.loginservicesUrl, this.formdata).map(res => res).toPromise();
    }
    getserviceanalytics(fromDate,toDate){
        this.formdata = new FormData();
        this.formdata.append('action', 'getServicelog');
        this.formdata.append('fromDate', fromDate);
        this.formdata.append('toDate', toDate);
        return this.http.post(this.loginservicesUrl, this.formdata).map(res => res).toPromise();
    }

    sendpushnotitfications(title, message) {
        // this.headers.append('Content-Type',"application/json");
        // this.headers.append('Authorization',"key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr");
        // this.headers.append('Content-Type',"application/json");
        this.headers.set('Authorization', "key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr");



        // var settings = {
        //     "async": true,
        //     "crossDomain": true,
        //     "url": "https://fcm.googleapis.com/fcm/send",
        //     "method": "POST",
        //     "headers": {
        //       "Content-Type": "application/json",
        //       "Authorization": "key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr",
        //       "Cache-Control": "no-cache",
        //       "Postman-Token": "932d1151-9646-dfeb-d959-14e40502d3be"
        //     },
        //     "processData": false,
        //     "data": "{ \n     \"to\": \"cNHSLR6lrt0:APA91bGbhcCukJTG9_XCT4EhvdsCri5NUhi5gjUfYJ-WdPF7a7gUXWHVO2WDaRyqCblLU-QNjDL728nlIZxAYolVGBWBokqwfj34t4eQeYnXlZPNAHwEiKT7XQlHolh4KM893CqYndJj\",  \n     \"notification\" : {\n     \"body\" : \"message\",\n     \"content_available\" : true,\n     \"priority\" : \"high\",\n     \"title\" : \"title of the notification\"\n     },\n    \"data\":{\n    \"title\":\"title of the notification\",\n    \"message\":\"description of the notification\"\n  }\n}"
        //   }

        //   let data = {
        //         "to": "cNHSLR6lrt0:APA91bGbhcCukJTG9_XCT4EhvdsCri5NUhi5gjUfYJ-WdPF7a7gUXWHVO2WDaRyqCblLU-QNjDL728nlIZxAYolVGBWBokqwfj34t4eQeYnXlZPNAHwEiKT7XQlHolh4KM893CqYndJj",  
        //         "notification" : {
        //         "body" : "message",
        //         "content_available" : true,
        //         "priority" : "high",
        //         "title" : "title of the notification"
        //         },
        //        "data":{
        //        "title": title,
        //        "message": message
        //      }
        //   } 
        //   this.options.params = data;


        //     return this.http.post('https://fcm.googleapis.com/fcm/send', new Headers({
        //         'Content-Type': 'application/json',
        //         'Access-Control-Allow-Origin': '*',
        //         'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
        //         'Accept': 'application/json',
        //         "Access-Control-Allow-Headers": "X-Requested-With",
        //         'Authorization':'key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr'

        //     }), this.options).map(res => res).toPromise();
        var notificationData = {
            "to": "eP7IBttrAUs:APA91bHar6vXqOxV3yAd-1jafdv8fMH7Wsqc7Q2Jzh4DN8n1jwir_YTqbxuwDOzBMBvBHS9jjKuTbtbVf82yTQP9fBsdPg1qN4h_gRAwQ_qqZBxt2tjXxqQQMcOhwoa7fWAbBpq883zV",
            "data": {
                "title": title,
                "message": message
            },
            "notification": {
                "color": "#FF0000",
                "title": title,
                "message": message
            }
        }

        $.ajax({
            url: 'https://fcm.googleapis.com/fcm/send',
            type: 'post',
            data: JSON.stringify(notificationData),
            headers: {
                'Content-Type': 'application/json',
                "Authorization": "key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr"
            },
            dataType: 'json',
            success: function (data) {
                console.info(data);
            }
        });

    }

    sendpushnotitficationsIos(username, title, message, platform, fcmtoken, applicationuuid, callbackmethod) {
        let appname;
        let authkey;
        if (applicationuuid === 'ae9055e5-210e-4a0b-9d39-b334c12cf744') {
            appname = 'tryg';
            if (platform === 'ios') {
                authkey = 'key=AAAAdaqE_1g:APA91bGO94NqysiCWW10UF6bO3SUl-nPsf3417nDUgxSfLI1E1hj9VhF0FqKF4SQxLpgXJOrJv3bQtrSnIgc6FEkILmkMfg5LT-lHaBSLJkvNVzk44RTU7oP0AyZPSUz-fncaBGo-zqA';
            }
            else {
                authkey = 'key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr';
            }

        }
        else if (applicationuuid === 'dc252eac-7759-4de1-a371-25da7a76761b') {
            appname = 'molholm';
            if (platform === 'ios') {
                authkey = 'key=AAAA6GCeCiU:APA91bGku6Clpq4pxSk62uDO66w7c5qx19JhSaAVtB9AQi9yzYzGhDLUV6M6lATJoWha5zSxnfXNt0qW8PiUL6ufmfBihzTDmrvcyfO8KdPLcjoBTqt88JQWHW12J7u5xy5Gnx7u2QpI';
            }
            else {
                authkey = 'key=AAAAEm0aSNM:APA91bEai1oBrw7uj6Z7U5bfmJljKKObTuNDGsgkTQ1LCefuNMxrSGpqBjQ1wXWltC8rYiqFdvaUZI81zzyVX3VCN6QRiN2zzvoav6GpBfvHzKR8vnFDA5r12lT8pYLNpcbbO16XQin3';
            }

        }
        else if (applicationuuid === 'ae9055e5-210e-4a0b-9d39-b334c12cf744') {
            appname = 'willis';
            if (platform === 'ios') {
                authkey = 'key=AAAAdaqE_1g:APA91bGO94NqysiCWW10UF6bO3SUl-nPsf3417nDUgxSfLI1E1hj9VhF0FqKF4SQxLpgXJOrJv3bQtrSnIgc6FEkILmkMfg5LT-lHaBSLJkvNVzk44RTU7oP0AyZPSUz-fncaBGo-zqA';
            }
            else {
                authkey = 'key=AAAAmbkezF4:APA91bG5pJfArZEMdheKC7RsdyO0xGR7wgi2GmHnn0xO7QDLmF_4R2BjBuHpcooxAZWGOCBfAAvPH6jEhgIaCFE9s3kB-eLC8Kis9xwGL_-7oxhKzS3s_4-Jr-lBWAM5GBHuSOYYDryr';
            }

        }
        else if (applicationuuid === '86d383ea-644c-49df-9d8c-0fdde0811a35') {
            appname = 'ap';
            if (platform === 'ios') {
                authkey = 'key=AAAA8MKLg04:APA91bGG-HtxaKLjS2DC8rHWdcG98yj07gXlqAvkLRF2lp_JHfUBL7NhTGiEtSYXenlPgj2Zt2PE79BlOVIGyE6g2zcklTvnefJZWc0F8lfZcFjSqC7X1yM9v6ffmLufCbsKp_SvN3Xi';
            }
            else {
                authkey = 'key=AAAAI6sp4xM:APA91bFFiRiCVlcYGmNbAxfTW9QixuRYKivvHsZ4dakVathOw4lLGvDPY3BAWYo3N_swTW46YHminUibfWTlNLBCGaSN44Ir3wYtrTDLsicnaTVTMCXmp9PbzzaTsSNAUS_2ExTqgznd';
            }

        }
        var notificationData = {
            "content_available": true,
            "priority": "high",
            "to": fcmtoken,
            "data": {
                "title": title,
                "body": message
            },
            "notification": {
                "title": title,
                "body": message,
                "click_action": "FCM_PLUGIN_ACTIVITY"
            }
        }
        let self = this;
        $.ajax({
            url: 'https://fcm.googleapis.com/fcm/send',
            context: this,
            type: 'post',
            async: true,
            data: JSON.stringify(notificationData),
            headers: {
                'Content-Type': 'application/json',
                "Authorization": authkey
            },
            dataType: 'json',
            success: function (data) {
                console.info(data);
                callbackmethod(username, data, self);
            }.bind(this)
        });

    }
    sendpushnotitficationsWeb(title, message) {

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://fcm.googleapis.com/fcm/send",
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "Authorization": "key=AAAAGyTHDo8:APA91bHoRWfi8F3VLPPjKWsYXWMko-YeHKVyZ9Tyec5CZJCZPGllP4LDgoFBerJhI8eGHwNW9LiPO-w1iCei1V-G9oFUGE6gfV2oQgRCzIQbJ1T4XlEVFRjZd9SXD-XHxUXyvrRpI1qQ",
                "Cache-Control": "no-cache",
                "Postman-Token": "c822701e-ef4b-0ba7-b1b9-455afe7b7d56"
            },
            "processData": false,
            "data": "{ \n     \n     \"notification\" : {\n     \"body\" : \"Tryg Sundhed \",\n     \"content_available\" : true,\n     \"priority\" : \"high\",\n     \"title\" : \"Notification from tryg\"\n     },\n     \"to\": \"f1kmHL65OYI:APA91bEl7vrwiilrq9XaE-nADq77lyKHQ1diGQKFWw5oTDL56i27U8wnumtrOPVtatxiBKjUCwP7JUjDHsQvZPIIw5-dioomkZEDXKIATafNEy0X6yRshMSJU8UjwNc8ripENKi29_TH\"\n}"
        }

        $.ajax(settings).done(function (response) {
            console.log(response);
        });
    }
    translateImport(file,filename,authorisation, objekttype, organisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'importTranslations');
        this.formdata.append('autorisation', authorisation);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    translateExport(authorisation, objekttype, organisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'exportTranslations');
        this.formdata.append('autorisation', authorisation);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    getskabelonlist(organisationsid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabelon');
        this.formdata.append('organisationsid', organisationsid);
        return this.http.post(this.skabelonUrl, this.formdata).map(res => res).toPromise();

    }
    getskabeloncontent(skabelonid,lang) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabeloncontent');
        // this.formdata.append('organisationsid', organisationsid);
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('sprog', lang);
        return this.http.post(this.skabelonUrl, this.formdata).map(res => res).toPromise();

    }
    saveskabeloncontent(skabelonid,title, teaser,beskrivelse,flettefelter, slctlang,body) {
        this.formdata = new FormData();
        this.formdata.append('action', 'saveskabeloncontent');
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('titel', title);
        this.formdata.append('teaser', teaser);
        this.formdata.append('beskrivelse', beskrivelse);
        this.formdata.append('sprog', slctlang);
        this.formdata.append('flettefelter', flettefelter);
        this.formdata.append('tekst', body);
        return this.http.post(this.skabelonUrl, this.formdata).map(res => res).toPromise();

    }
    getalldevices() {
        this.formdata = new FormData();
        this.formdata.append('action', 'getalldevices');
        return this.http.post(this.loginservicesUrl, this.formdata, this.options).toPromise();
    }
    login(username, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', username);
        this.formdata.append('password', password);
        return this.http.post('https://uat-idp.carex.dk/endpoints/login_services.php', this.formdata, this.options).toPromise();
    }


}