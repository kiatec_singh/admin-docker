<?php
header('Access-Control-Allow-Origin: *');
include("../include/db_mysqli.php");
session_start();
require_once ('/var/simplesamlphp/lib/_autoload.php');
$as = new SimpleSAML_Auth_Simple('idp.carex.dk');
$as->requireAuth();

$attributes = $as->getAttributes();

$json =json_encode($attributes);

//$json = str_replace('"', '\'', $json);

?>





<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Carex Admin</title>
  <base href="/">
     <script type="text/javascript" src="assets/lib/loader.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
  <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/charts/ammap.js"></script>
    <script src="assets/lib/charts/worldLow.js"></script>
     <script src="assets/lib/charts/light.js"></script> 
     <script src="assets/lib/charts/amcharts.js"></script> 
     <script src="assets/lib/charts/export.min.js"></script>
     <script src="assets/lib/charts/jszip.min.js"></script>
     <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" /> 

   
     <script src="assets/lib/popper.min.js"></script>
    <script src="assets/lib/bootstrap.min.js"></script>
   <script src="assets/lib/bootstrap-select.min.js"></script>

   <script src="https://molholmsundhedsunivers.carex.dk/js/fontawesome.js"></script> 
    <!-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-Bx4pytHkyTDy3aJKjGkGoHPt3tvv6zlwwjc3iqN7ktaiEMLDPqLSZYts2OjKcBx1" crossorigin="anonymous"> -->
</head>
  <body>
      <input type="hidden" id="userinfo" name="uuid" value="<?php echo htmlentities($json);?>">
      
   
      
      <app-root></app-root>
      <script src="runtime-es2015.js" type="module"></script><script src="runtime-es5.js" nomodule defer></script><script src="polyfills-es5.js" nomodule defer></script><script src="polyfills-es2015.js" type="module"></script><script src="styles-es2015.js" type="module"></script><script src="styles-es5.js" nomodule defer></script><script src="vendor-es2015.js" type="module"></script><script src="vendor-es5.js" nomodule defer></script><script src="main-es2015.js" type="module"></script><script src="main-es5.js" nomodule defer></script></body>    
    </html>